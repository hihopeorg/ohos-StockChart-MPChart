# StockChart-MPohosChart

**本项目是基于开源项目StockChart-MPohosChart进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/LambdaXiao/StockChart-MPAndroidChart ）追踪到原项目版本**

#### 项目介绍

- 项目名称：基于MPLibChart的专业股票图，如分时图和K线图KLine
- 所属系列：ohos的第三方组件移植
- 功能：金融图表库
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/LambdaXiao/StockChart-MPAndroidChart
- 原项目基线版本：v1.2 ,sha1:224c9e5f4b2a91bd9bf75f88d439146fa9aa9a9a
- 编程语言：Java
- 外部库依赖：无

#### 效果展示
<img src="screenshot/统计图.gif"/>
<img src="screenshot/股票图.gif"/>

#### 安装教程

方法1.

1. 编译依赖库har包MPChartLib.har。

2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

   ```
   dependencies {
       implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
   	……
   }
   ```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

   ```
   repositories {
       maven {
           url 'http://106.15.92.248:8081/repository/Releases/' 
       }
   }。
   ```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

   ```
   dependencies {
          implementation 'com.github.mikephil.charting.ohos:MPChartLib:1.0.1'
   }
   ```

#### 使用说明

 1.在布局文件中引用自定义Chart视图
   ```
         <com.github.mikephil.charting.charts.BarChart
         ohos:id="$+id:chart1"
         ohos:height="match_parent"
         ohos:width="match_parent"/>
   ```

 2.在代码中为Chart设置属性
   ```
        chart = (BarChart) findComponentById(ResourceTable.Id_chart1);
        chart.setOnChartValueSelectedListener(this);
        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);
        chart.getDescription().setEnabled(false);
        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(60);
        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);
        chart.setDrawGridBackground(false);
        // chart.setDrawYLabels(false);
   ```

#### 版本迭代
- v1.0.1
  
#### 版权和许可信息

```
MIT License

Copyright (c) 2019 Lambda Xiao

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

