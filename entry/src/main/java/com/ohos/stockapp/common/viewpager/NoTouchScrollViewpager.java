package com.ohos.stockapp.common.viewpager;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public class NoTouchScrollViewpager extends PageSlider implements Component.TouchEventListener {
    public NoTouchScrollViewpager(Context context) {
        super(context);
        setTouchEventListener(this);
    }

    public NoTouchScrollViewpager(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setTouchEventListener(this);
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return false;
    }
}
