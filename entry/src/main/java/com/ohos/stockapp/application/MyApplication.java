package com.ohos.stockapp.application;

import com.github.mikephil.charting.utils.Utils;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        Utils.init(getContext());
    }
}
