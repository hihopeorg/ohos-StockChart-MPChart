package com.ohos.stockapp.ui.market.fragment;

import com.ohos.stockapp.ResourceTable;

import com.ohos.stockapp.ui.base.BaseFragment;
import com.ohos.stockapp.common.data.ChartData;
import com.ohos.stockapp.ui.market.activity.StockDetailAbility;
import com.ohos.stockapp.ui.market.activity.StockDetailLandAbility;
import com.github.mikephil.charting.stockChart.OneDayChart;
import com.github.mikephil.charting.stockChart.charts.CoupleChartGestureListener;
import com.github.mikephil.charting.stockChart.dataManage.TimeDataManage;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.utils.zson.ZSONException;
import ohos.utils.zson.ZSONObject;

public class ChartOneDayFragment extends BaseFragment {
    OneDayChart chart;
    private  boolean land;//是否横屏
    private TimeDataManage kTimeData = new TimeDataManage();
    private ZSONObject object;

    public static ChartOneDayFragment newInstance(boolean lad) {
        ChartOneDayFragment fragment = new ChartOneDayFragment();
        fragment.land = lad;
        return fragment;

    }

    @Override
    protected int setLayoutId() {
        return ResourceTable.Layout_fragment_one_day;
    }

    @Override
    protected void initBase(Component view) {
        chart = (OneDayChart) view.findComponentById(ResourceTable.Id_chart);
        //初始化
        chart.initChart(land);
        //测试数据
        try {
            object = ZSONObject.stringToZSON(ChartData.getTIMEDATA());
        } catch (ZSONException e) {
            e.printStackTrace();
        }

        //上证指数代码000001.IDX.SH
        kTimeData.parseTimeData(object, "000001.IDX.SH", 0);
        chart.setDataToChart(kTimeData);

        //非横屏页单击转横屏页
        if (!land) {
            chart.getGestureListenerLine().setCoupleClick(new CoupleChartGestureListener.CoupleClick() {
                @Override
                public void singleClickListener() {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName("com.ohos.stockapp")
                            .withAbilityName(StockDetailLandAbility.class.getName())
                            .build();
                    intent.setOperation(operation);
                    getFractionAbility().startAbility(intent);
                    onStop();
                }
            });
        }else{
            chart.getGestureListenerBar().setCoupleClick(new CoupleChartGestureListener.CoupleClick() {
                @Override
                public void singleClickListener() {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName("com.ohos.stockapp")
                            .withAbilityName(StockDetailAbility.class.getName())
                            .build();
                    intent.setOperation(operation);
                    getFractionAbility().startAbility(intent);
                    onStop();
                }
            });
        }
    }
}
