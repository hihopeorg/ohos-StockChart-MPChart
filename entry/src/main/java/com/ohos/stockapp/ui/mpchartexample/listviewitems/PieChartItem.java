
package com.ohos.stockapp.ui.mpchartexample.listviewitems;


import com.ohos.stockapp.ResourceTable;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.formatter.PercentFormatter;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class PieChartItem extends ChartItem {
    //TODO Typeface
//    private final Typeface mTf;
    private final String mCenterText;

    public PieChartItem(ChartData<?> cd, Context c) {
        super(cd);

//        mTf = Typeface.createFromAsset(c.getAssets(), "OpenSans-Regular.ttf");
        mCenterText = generateCenterText();
    }

    @Override
    public int getItemType() {
        return TYPE_PIECHART;
    }


    @Override
    public Component getView(int position, Component convertView, Context c) {

        ViewHolder holder ;

//        if (convertView == null) {

            holder = new ViewHolder();

            convertView = LayoutScatter.getInstance(c).parse(
                    ResourceTable.Layout_list_item_piechart, null,false);
            holder.chart = (PieChart) convertView.findComponentById(ResourceTable.Id_chart);

            convertView.setTag(holder);

//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }

        // apply styling
        holder.chart.getDescription().setEnabled(false);
        holder.chart.setHoleRadius(52f);
        holder.chart.setTransparentCircleRadius(57f);
        holder.chart.setCenterText(mCenterText);
//        holder.chart.setCenterTextTypeface(mTf);
        holder.chart.setCenterTextSize(9f);
        holder.chart.setUsePercentValues(true);
        holder.chart.setExtraOffsets(5, 10, 50, 10);

        mChartData.setValueFormatter(new PercentFormatter());
//        mChartData.setValueTypeface(mTf);
        mChartData.setValueTextSize(11f);
        mChartData.setValueTextColor(Color.WHITE.getValue());
        // set data
        holder.chart.setData((PieData) mChartData);

        Legend l = holder.chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // do not forget to refresh the chart
        // holder.chart.invalidate();
        holder.chart.animateY(900);

        return convertView;
    }

    private String generateCenterText() {
        StringBuffer s = new StringBuffer("MPohosChart\ncreated by\nPhilipp Jahoda");
        return s.toString();
    }

    private static class ViewHolder {
        PieChart chart;
    }
}
