package com.ohos.stockapp.ui.market.fragment;


import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.base.BaseFragment;
import com.ohos.stockapp.common.data.ChartData;
import com.ohos.stockapp.ui.market.activity.StockDetailAbility;
import com.ohos.stockapp.ui.market.activity.StockDetailLandAbility;
import com.github.mikephil.charting.stockChart.FiveDayChart;
import com.github.mikephil.charting.stockChart.charts.CoupleChartGestureListener;
import com.github.mikephil.charting.stockChart.dataManage.TimeDataManage;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

import ohos.utils.zson.ZSONException;
import ohos.utils.zson.ZSONObject;

public class ChartFiveDayFragment extends BaseFragment {
    FiveDayChart chart;
    private  boolean land=false;//是否横屏
    private TimeDataManage kTimeData = new TimeDataManage();
    private ZSONObject object;

    public static ChartFiveDayFragment newInstance(boolean lad) {
        ChartFiveDayFragment fragment = new ChartFiveDayFragment();
        fragment.land=lad;
        return fragment;

    }
    @Override
    protected int setLayoutId() {
        return ResourceTable.Layout_frag_five_day;
    }

    @Override
    protected void initBase(Component view) {
        chart= (FiveDayChart) view.findComponentById(ResourceTable.Id_chart);
        chart.initChart(land);
        //测试数据
        try {
            object =ZSONObject.stringToZSON(ChartData.getFiveTIMEDATA());
        } catch (ZSONException e) {
            e.printStackTrace();
        }

        //上证指数代码000001.IDX.SH
        kTimeData.parseTimeData(object,"000001.IDX.SH",0);
        chart.setDataToChart(kTimeData);

        //非横屏页单击转横屏页
        if (!land) {
            chart.getGestureListenerLine().setCoupleClick(new CoupleChartGestureListener.CoupleClick() {
                @Override
                public void singleClickListener() {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName("com.ohos.stockapp")
                            .withAbilityName(StockDetailLandAbility.class.getName())
                            .build();
                    intent.setOperation(operation);
                    getFractionAbility().startAbility(intent);
                }
            });
            chart.getGestureListenerBar().setCoupleClick(new CoupleChartGestureListener.CoupleClick() {
                @Override
                public void singleClickListener() {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName("com.ohos.stockapp")
                            .withAbilityName(StockDetailAbility.class.getName())
                            .build();
                    intent.setOperation(operation);
                    getFractionAbility().startAbility(intent);
                }
            });
        }
    }
}
