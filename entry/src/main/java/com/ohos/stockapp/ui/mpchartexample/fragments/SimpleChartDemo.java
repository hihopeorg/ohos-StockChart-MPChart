
package com.ohos.stockapp.ui.mpchartexample.fragments;


import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.MyMarkerView;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.*;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.FileUtils;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;

import java.util.ArrayList;

/**
 * Demonstrates how to keep your charts straight forward, simple and beautiful with the MPohosChart library.
 *
 * @author Philipp Jahoda
 */
public class SimpleChartDemo extends DemoBase {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setLayoutFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN,);
        super.setUIContent(ResourceTable.Layout_activity_awesomedesign);

//        setTitle("SimpleChartDemo");

        PageSlider pager = (PageSlider) findComponentById(ResourceTable.Id_pager);

        PageAdapter a = new PageAdapter();
        pager.setProvider(a);


        CommonDialog b = new CommonDialog(getContext());
        b.setTitleText("This is a ViewPager.");
        b.setContentText("Swipe left and right for more awesome design examples!");
        b.setButton(1, "OK", new IDialog.ClickedListener() {
            @Override
            public void onClick(IDialog iDialog, int i) {
                iDialog.destroy();
            }

        });
        b.show();
    }

    private class PageAdapter extends PageSliderProvider {

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int i) {
            Component component ;

            switch (i) {
                case 0:
                    component= LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_frag_simple_line,null,false);
                    LineChart chart = (LineChart) component.findComponentById(ResourceTable.Id_lineChart1);

                    chart.getDescription().setEnabled(false);

                    chart.setDrawGridBackground(false);

                    chart.setData(generateLineData());
                    chart.animateX(3000);

                    //TODO  Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

                    Legend l = chart.getLegend();
                    //l.setTypeface(tf);
                    YAxis leftAxis = chart.getAxisLeft();
                    //leftAxis.setTypeface(tf);
                    leftAxis.setAxisMaximum(1.2f);
                    leftAxis.setAxisMinimum(-1.2f);
                    chart.getAxisRight().setEnabled(false);
                    XAxis xAxis = chart.getXAxis();
                    xAxis.setEnabled(false);

                    break;
                case 1:
                    component= LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_frag_simple_line,null,false);
                   LineChart chart1 = (LineChart) component.findComponentById(ResourceTable.Id_lineChart1);

                    chart1.getDescription().setEnabled(false);

                    chart1.setDrawGridBackground(false);

                    chart1.setData(getComplexity());
                    chart1.animateX(3000);

                    //todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

                    Legend l1 = chart1.getLegend();
                    //        l.setTypeface(tf);

                    YAxis leftAxis1 = chart1.getAxisLeft();
                    //leftAxis.setTypeface(tf);

                    chart1.getAxisRight().setEnabled(false);

                    XAxis xAxis1 = chart1.getXAxis();
                    xAxis1.setEnabled(false);

                    break;
                case 2:
                    component= LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_frag_simple_bar,null,false);
                    // create a new chart object
                    BarChart chart2 = (BarChart) component.findComponentById(ResourceTable.Id_bar_chart);new BarChart(getContext());
                    chart2.getDescription().setEnabled(false);
//                    chart2.setOnChartGestureListener(this);

                    MyMarkerView mv = new MyMarkerView(getContext(), ResourceTable.Layout_custom_marker_view);
                    mv.setChartView(chart2); // For bounds control
                    chart2.setMarker(mv);
                    chart2.setDrawGridBackground(false);
                    chart2.setDrawBarShadow(false);

                    //todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

                    chart2.setData(generateBarData(1, 20000, 12));

                    Legend l2 = chart2.getLegend();
                    //l.setTypeface(tf);

                    YAxis leftAxis2 = chart2.getAxisLeft();
                    //leftAxis.setTypeface(tf);
                    leftAxis2.setAxisMinimum(0f); // this replaces setStartAtZero(true)

                    chart2.getAxisRight().setEnabled(false);

                    XAxis xAxis2 = chart2.getXAxis();
                    xAxis2.setEnabled(false);

                    // programmatically add the chart
//                    StackLayout parent = (StackLayout) component.findComponentById(ResourceTable.Id_parentLayout);
//                    parent.addComponent(chart2);
                    break;
                case 3:
                    component= LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_frag_simple_scatter,null,false);
                    ScatterChart chart3 = (ScatterChart) component.findComponentById(ResourceTable.Id_scatterChart1);
                    chart3.getDescription().setEnabled(false);

                    //todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

                    MyMarkerView mv3 = new MyMarkerView(getContext(), ResourceTable.Layout_custom_marker_view);
                    mv3.setChartView(chart3); // For bounds control
                    chart3.setMarker(mv3);

                    chart3.setDrawGridBackground(false);
                    chart3.setData(generateScatterData(6, 10000, 200));

                    XAxis xAxis3 = chart3.getXAxis();
                    xAxis3.setEnabled(true);
                    xAxis3.setPosition(XAxis.XAxisPosition.BOTTOM);

                    YAxis leftAxis3 = chart3.getAxisLeft();
                    //leftAxis.setTypeface(tf);

                    YAxis rightAxis = chart3.getAxisRight();
                    //rightAxis.setTypeface(tf);
                    rightAxis.setDrawGridLines(false);

                    Legend l3 = chart3.getLegend();
                    l3.setWordWrapEnabled(true);
                    //l.setTypeface(tf);
                    l3.setFormSize(14f);
                    l3.setTextSize(9f);

                    // increase the space between legend & bottom and legend & content
                    l3.setYOffset(13f);
                    chart3.setExtraBottomOffset(16f);
                    break;
                case 4:
                    component= LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_frag_simple_pie,null,false);
                    PieChart chart4 = (PieChart) component.findComponentById(ResourceTable.Id_pieChart1);
                    chart4.getDescription().setEnabled(false);

                    //todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

                    //chart.setCenterTextTypeface(tf);
                    //chart.setCenterText(generateCenterText());
                    chart4.setCenterTextSize(10f);
                    //chart.setCenterTextTypeface(tf);

                    // radius of the center hole in percent of maximum radius
                    chart4.setHoleRadius(45f);
                    chart4.setTransparentCircleRadius(50f);

                    Legend l4 = chart4.getLegend();
                    l4.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
                    l4.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
                    l4.setOrientation(Legend.LegendOrientation.VERTICAL);
                    l4.setDrawInside(false);

                    chart4.setData(generatePieData());
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + i);
            }
            componentContainer.addComponent(component);
            return component;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
            componentContainer.removeComponent((Component) o);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object o) {
            return true;
        }
    }

    public BarData generateBarData(int dataSets, float range, int count) {

        ArrayList<IBarDataSet> sets = new ArrayList<>();

        for(int i = 0; i < dataSets; i++) {

            ArrayList<BarEntry> entries = new ArrayList<>();

            for(int j = 0; j < count; j++) {
                entries.add(new BarEntry(j, (float) (Math.random() * range) + range / 4));
            }

            BarDataSet ds = new BarDataSet(entries, getLabel(i));
            ds.setColors(ColorTemplate.VORDIPLOM_COLORS);
            sets.add(ds);
        }

        BarData d = new BarData(sets);
//        d.setValueTypeface(tf);
        return d;
    }

    public ScatterData generateScatterData(int dataSets, float range, int count) {

        ArrayList<IScatterDataSet> sets = new ArrayList<>();

        ScatterChart.ScatterShape[] shapes = ScatterChart.ScatterShape.getAllDefaultShapes();

        for(int i = 0; i < dataSets; i++) {

            ArrayList<Entry> entries = new ArrayList<>();

            for(int j = 0; j < count; j++) {
                entries.add(new Entry(j, (float) (Math.random() * range) + range / 4));
            }

            ScatterDataSet ds = new ScatterDataSet(entries, getLabel(i));
            ds.setScatterShapeSize(12f);
            ds.setScatterShape(shapes[i % shapes.length]);
            ds.setColors(ColorTemplate.COLORFUL_COLORS);
            ds.setScatterShapeSize(9f);
            sets.add(ds);
        }

        ScatterData d = new ScatterData(sets);
//        d.setValueTypeface(tf);
        return d;
    }

    /**
     * generates less data (1 DataSet, 4 values)
     * @return PieData
     */
    public PieData generatePieData() {

        int count = 4;

        ArrayList<PieEntry> entries1 = new ArrayList<>();

        for(int i = 0; i < count; i++) {
            entries1.add(new PieEntry((float) ((Math.random() * 60) + 40), "Quarter " + (i+1)));
        }

        PieDataSet ds1 = new PieDataSet(entries1, "Quarterly Revenues 2015");
        ds1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        ds1.setSliceSpace(2f);
        ds1.setValueTextColor(Color.WHITE.getValue());
        ds1.setValueTextSize(12f);

        PieData d = new PieData(ds1);
//        d.setValueTypeface(tf);

        return d;
    }

    public LineData generateLineData() {

        ArrayList<ILineDataSet> sets = new ArrayList<>();
        LineDataSet ds1 = new LineDataSet(FileUtils.loadEntriesFromAssets(getResourceManager(), "resources/rawfile/sine.txt"), "Sine function");
        LineDataSet ds2 = new LineDataSet(FileUtils.loadEntriesFromAssets(getResourceManager(), "resources/rawfile/cosine.txt"), "Cosine function");

        ds1.setLineWidth(2f);
        ds2.setLineWidth(2f);

        ds1.setDrawCircles(false);
        ds2.setDrawCircles(false);

        ds1.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);
        ds2.setColor(ColorTemplate.VORDIPLOM_COLORS[1]);

        // load DataSets from files in assets folder
        sets.add(ds1);
        sets.add(ds2);

        LineData d = new LineData(sets);
//        d.setValueTypeface(tf);
        return d;
    }

    public LineData getComplexity() {

        ArrayList<ILineDataSet> sets = new ArrayList<>();

        LineDataSet ds1 = new LineDataSet(FileUtils.loadEntriesFromAssets(getResourceManager(), "resources/rawfile/n.txt"), "O(n)");
        LineDataSet ds2 = new LineDataSet(FileUtils.loadEntriesFromAssets(getResourceManager(), "resources/rawfile/nlogn.txt"), "O(nlogn)");
        LineDataSet ds3 = new LineDataSet(FileUtils.loadEntriesFromAssets(getResourceManager(), "resources/rawfile/square.txt"), "O(n\u00B2)");
        LineDataSet ds4 = new LineDataSet(FileUtils.loadEntriesFromAssets(getResourceManager(), "resources/rawfile/three.txt"), "O(n\u00B3)");

        ds1.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);
        ds2.setColor(ColorTemplate.VORDIPLOM_COLORS[1]);
        ds3.setColor(ColorTemplate.VORDIPLOM_COLORS[2]);
        ds4.setColor(ColorTemplate.VORDIPLOM_COLORS[3]);

        ds1.setCircleColor(ColorTemplate.VORDIPLOM_COLORS[0]);
        ds2.setCircleColor(ColorTemplate.VORDIPLOM_COLORS[1]);
        ds3.setCircleColor(ColorTemplate.VORDIPLOM_COLORS[2]);
        ds4.setCircleColor(ColorTemplate.VORDIPLOM_COLORS[3]);

        ds1.setLineWidth(2.5f);
        ds1.setCircleRadius(3f);
        ds2.setLineWidth(2.5f);
        ds2.setCircleRadius(3f);
        ds3.setLineWidth(2.5f);
        ds3.setCircleRadius(3f);
        ds4.setLineWidth(2.5f);
        ds4.setCircleRadius(3f);


        // load DataSets from files in assets folder
        sets.add(ds1);
        sets.add(ds2);
        sets.add(ds3);
        sets.add(ds4);

        LineData d = new LineData(sets);
//        d.setValueTypeface(tf);
        return d;
    }

    private final String[] mLabels = new String[] { "Company A", "Company B", "Company C", "Company D", "Company E", "Company F" };

    private String getLabel(int i) {
        return mLabels[i];
    }

    @Override
    public void saveToGallery() { /* Intentionally left empty */ }
}
