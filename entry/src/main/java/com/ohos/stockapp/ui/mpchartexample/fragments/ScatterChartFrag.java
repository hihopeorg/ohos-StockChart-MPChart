package com.ohos.stockapp.ui.mpchartexample.fragments;
import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.MyMarkerView;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;


public class ScatterChartFrag extends SimpleFragment {

    public static Fraction newInstance() {
        return new ScatterChartFrag();
    }

    @SuppressWarnings("FieldCanBeLocal")
    private ScatterChart chart;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component v = scatter.parse(ResourceTable.Layout_frag_simple_scatter, container, false);

        chart = (ScatterChart) v.findComponentById(ResourceTable.Id_scatterChart1);
        chart.getDescription().setEnabled(false);

//todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

        MyMarkerView mv = new MyMarkerView(getFractionAbility(), ResourceTable.Layout_custom_marker_view);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv);

        chart.setDrawGridBackground(false);
        chart.setData(generateScatterData(6, 10000, 200));

        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setPosition(XAxisPosition.BOTTOM);

        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(tf);

        YAxis rightAxis = chart.getAxisRight();
//        rightAxis.setTypeface(tf);
        rightAxis.setDrawGridLines(false);

        Legend l = chart.getLegend();
        l.setWordWrapEnabled(true);
//        l.setTypeface(tf);
        l.setFormSize(14f);
        l.setTextSize(9f);

        // increase the space between legend & bottom and legend & content
        l.setYOffset(13f);
        chart.setExtraBottomOffset(16f);

        return v;
    }
}
