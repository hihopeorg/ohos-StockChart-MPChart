package com.ohos.stockapp.ui.main;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.market.activity.StockDetailAbility;
import com.ohos.stockapp.ui.mpchartexample.notimportant.MPMainAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_btn_test).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(StockDetailAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
        findComponentById(ResourceTable.Id_btn_mp).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(MPMainAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
            }
        });
    }
}
