package com.ohos.stockapp.ui.base;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public abstract class BaseFragment extends Fraction {
    private Component view;
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component=scatter.parse(setLayoutId(),container,false);
        initBase(component);
        return component;
    }
    protected abstract int setLayoutId();

    protected abstract void initBase(Component view);
    protected void startToActivity(Class<?> cls){

        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.ohos.stockapp")
                .withAbilityName(cls)
                .build();
        intent.setOperation(operation);
        getFractionAbility().startAbility(intent);
    }
    ToastDialog toast;
    public void showToast(String msg){
        if(toast == null){
            toast = new ToastDialog(getContext()).setText(msg);
        }else{
            toast.setText(msg);
        }
        toast.show();
    }

    @Override
    protected void onStop() {
        if(toast!=null)
            toast.cancel();
        super.onStop();
    }

    @Override
    public void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
