package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.RadarMarkerView;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.utils.net.Uri;

import java.util.ArrayList;

public class RadarChartAbility extends DemoBase {
    private RadarChart chart;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_radar_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("RadarChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("radar"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);
                                break;
                            case "actionToggleValues":
                                for (IDataSet<?> set : chart.getData().getDataSets())
                                    set.setDrawValues(!set.isDrawValuesEnabled());
                                chart.invalidate();
                                break;
                            case "actionToggleHighlight": {
                                if (chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;
                            }
                            case "actionToggleRotate": {
                                if (chart.isRotationEnabled())
                                    chart.setRotationEnabled(false);
                                else
                                    chart.setRotationEnabled(true);
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleFilled": {
                                ArrayList<IRadarDataSet> sets = (ArrayList<IRadarDataSet>) chart.getData()
                                        .getDataSets();

                                for (IRadarDataSet set : sets) {
                                    if (set.isDrawFilledEnabled())
                                        set.setDrawFilled(false);
                                    else
                                        set.setDrawFilled(true);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleHighlightCircle": {
                                ArrayList<IRadarDataSet> sets = (ArrayList<IRadarDataSet>) chart.getData()
                                        .getDataSets();

                                for (IRadarDataSet set : sets) {
                                    set.setDrawHighlightCircleEnabled(!set.isDrawHighlightCircleEnabled());
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleXLabels": {
                                chart.getXAxis().setEnabled(!chart.getXAxis().isEnabled());
                                chart.notifyDataSetChanged();
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleYLabels": {

                                chart.getYAxis().setEnabled(!chart.getYAxis().isEnabled());
                                chart.invalidate();
                                break;
                            }
                            case "animateX": {
                                chart.animateX(1400);
                                break;
                            }
                            case "animateY": {
                                chart.animateY(1400);
                                break;
                            }
                            case "animateXY": {
                                chart.animateXY(1400, 1400);
                                break;
                            }
                            case "actionToggleSpin": {
                                chart.spin(2000, chart.getRotationAngle(), chart.getRotationAngle() + 360, Easing.EaseInOutCubic);
                                break;
                            }
                            case "actionSave": {
                                saveToGallery();
                                break;
                            }
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });
    }

    @Override
    protected void onActive() {
        super.onActive();
        chart = (RadarChart) findComponentById(ResourceTable.Id_chart1);
        ShapeElement shapeElement=new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(Color.rgb(60, 65, 82)));
        chart.setBackground(shapeElement);

        chart.getDescription().setEnabled(false);

        chart.setWebLineWidth(1f);
        chart.setWebColor(Color.LTGRAY.getValue());
        chart.setWebLineWidthInner(1f);
        chart.setWebColorInner(Color.LTGRAY.getValue());
        chart.setWebAlpha(100);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MarkerView mv = new RadarMarkerView(this, ResourceTable.Layout_radar_markerview);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        setData();

        chart.animateXY(1400, 1400, Easing.EaseInOutQuad);

        XAxis xAxis = chart.getXAxis();
//        xAxis.setTypeface(tfLight);
        xAxis.setTextSize(9f);
        xAxis.setYOffset(0f);
        xAxis.setXOffset(0f);
        xAxis.setValueFormatter(new ValueFormatter() {
            private final String[] mActivities = new String[]{"Burger", "Steak", "Salad", "Pasta", "Pizza"};
            @Override
            public String getFormattedValue(float value) {
                return mActivities[(int) value % mActivities.length];
            }
        });
        xAxis.setTextColor(Color.WHITE.getValue());
        YAxis yAxis = chart.getYAxis();
//        yAxis.setTypeface(tfLight);
        yAxis.setLabelCount(5, false);
        yAxis.setTextSize(9f);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(80f);
        yAxis.setDrawLabels(false);
        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
//        l.setTypeface(tfLight);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
        l.setTextColor(Color.WHITE.getValue());
    }

    private void setData() {
        float mul = 80;
        float min = 20;
        int cnt = 5;

        ArrayList<RadarEntry> entries1 = new ArrayList<>();
        ArrayList<RadarEntry> entries2 = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < cnt; i++) {
            float val1 = (float) (Math.random() * mul) + min;
            entries1.add(new RadarEntry(val1));

            float val2 = (float) (Math.random() * mul) + min;
            entries2.add(new RadarEntry(val2));
        }

        RadarDataSet set1 = new RadarDataSet(entries1, "Last Week");
        set1.setColor(Color.rgb(103, 110, 129));
        set1.setFillColor(Color.rgb(103, 110, 129));
        set1.setDrawFilled(true);
        set1.setFillAlpha(180);
        set1.setLineWidth(2f);
        set1.setDrawHighlightCircleEnabled(true);
        set1.setDrawHighlightIndicators(false);

        RadarDataSet set2 = new RadarDataSet(entries2, "This Week");
        set2.setColor(Color.rgb(121, 162, 175));
        set2.setFillColor(Color.rgb(121, 162, 175));
        set2.setDrawFilled(true);
        set2.setFillAlpha(180);
        set2.setLineWidth(2f);
        set2.setDrawHighlightCircleEnabled(true);
        set2.setDrawHighlightIndicators(false);

        ArrayList<IRadarDataSet> sets = new ArrayList<>();
        sets.add(set1);
        sets.add(set2);

        RadarData data = new RadarData(sets);
//        data.setValueTypeface(tfLight);
        data.setValueTextSize(8f);
        data.setDrawValues(false);
        data.setValueTextColor(Color.WHITE.getValue());

        chart.setData(data);
        chart.invalidate();
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "RadarChartActivity");
    }



}
