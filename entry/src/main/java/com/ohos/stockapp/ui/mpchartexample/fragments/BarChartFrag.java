package com.ohos.stockapp.ui.mpchartexample.fragments;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.MyMarkerView;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.MmiPoint;

public class BarChartFrag extends SimpleFragment implements OnChartGestureListener {
   HiLogLabel label=new HiLogLabel(HiLog.INFO,0x7637445,"Gesture");


    public static Fraction newInstance() {
        return new BarChartFrag();
    }

    private BarChart chart;
    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
       Component component=scatter.parse(ResourceTable.Layout_frag_simple_bar,container,false);
        // create a new chart object
        chart = new BarChart(getFractionAbility());
        chart.getDescription().setEnabled(false);
        chart.setOnChartGestureListener(this);

        MyMarkerView mv = new MyMarkerView(getFractionAbility(), ResourceTable.Layout_custom_marker_view);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv);
        chart.setDrawGridBackground(false);
        chart.setDrawBarShadow(false);

//todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

        chart.setData(generateBarData(1, 20000, 12));

        Legend l = chart.getLegend();
//        l.setTypeface(tf);

        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(tf);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(false);

        // programmatically add the chart
        StackLayout parent = (StackLayout) component.findComponentById(ResourceTable.Id_parentLayout);
        parent.addComponent(chart);
        return component;
    }

    @Override
    public void onChartGestureStart(MmiPoint me, ChartTouchListener.ChartGesture lastPerformedGesture) {
       HiLog.info(label,"START");
    }

    @Override
    public void onChartGestureEnd(MmiPoint me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        HiLog.info(label,"END");
        chart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MmiPoint me) {
        HiLog.info(label, "Chart long pressed.");
    }

    @Override
    public void onChartDoubleTapped(MmiPoint me) {
        HiLog.info(label, "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MmiPoint me) {
        HiLog.info(label, "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MmiPoint me1, MmiPoint me2, float velocityX, float velocityY) {
        HiLog.info(label, "Chart fling. VelocityX: " + velocityX + ", VelocityY: " + velocityY);
    }

    @Override
    public void onChartScale(MmiPoint me, float scaleX, float scaleY) {
        HiLog.info(label, "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(DragEvent me, float dX, float dY) {
        HiLog.info(label, "dX: " + dX + ", dY: " + dY);
    }
}
