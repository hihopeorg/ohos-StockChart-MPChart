package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.CustomScatterShapeRenderer;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IScatterDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.IDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class ScatterChartAbility extends DemoBase implements Slider.ValueChangedListener, OnChartValueSelectedListener {

    private ScatterChart chart;
    private Slider seekBarX, seekBarY;
    private Text tvX, tvY;
    private HiLogLabel lable=new HiLogLabel(HiLog.INFO,0x66234343, Chart.LOG_TAG);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_scatter_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("ScatterChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("scatter"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);
                                break;
                            case "actionToggleValues":{
                                List<IScatterDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (IScatterDataSet iSet : sets) {

                                    ScatterDataSet set = (ScatterDataSet) iSet;
                                    set.setDrawValues(!set.isDrawValuesEnabled());
                                }

                                chart.invalidate();
                                break;}
                            case "actionToggleHighlight": {
                                if(chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;
                            }
                            case "actionTogglePinch": {
                                if (chart.isPinchZoomEnabled())
                                    chart.setPinchZoom(false);
                                else
                                    chart.setPinchZoom(true);

                                chart.invalidate();
                                break;
                            }
                            case "actionToggleAutoScaleMinMax": {
                                chart.setAutoScaleMinMaxEnabled(!chart.isAutoScaleMinMaxEnabled());
                                chart.notifyDataSetChanged();
                                break;
                            }
                            case "animateX": {
                                chart.animateX(3000);
                                break;
                            }
                            case "animateY": {
                                chart.animateY(3000);
                                break;
                            }
                            case "animateXY": {

                                chart.animateXY(3000, 3000);
                                break;
                            }
                            case "actionSave": {
                                    saveToGallery();
                                break;
                            }
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        tvY = (Text) findComponentById(ResourceTable.Id_tvYMax);

        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarX.setValueChangedListener(this);

        seekBarY = (Slider) findComponentById(ResourceTable.Id_seekBar2);
        seekBarY.setValueChangedListener(this);

        chart = (ScatterChart) findComponentById(ResourceTable.Id_chart1);
        chart.getDescription().setEnabled(false);
        chart.setOnChartValueSelectedListener(this);

        chart.setDrawGridBackground(false);
        chart.setTouchEnabled(true);
        chart.setMaxHighlightDistance(50f);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);

        chart.setMaxVisibleValueCount(200);
        chart.setPinchZoom(true);

        seekBarX.setProgressValue(45);
        seekBarY.setProgressValue(100);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
//        l.setTypeface(tfLight);
        l.setXOffset(5f);

        YAxis yl = chart.getAxisLeft();
//        yl.setTypeface(tfLight);
        yl.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);

        XAxis xl = chart.getXAxis();
//        xl.setTypeface(tfLight);
        xl.setDrawGridLines(false);

    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "ScatterChartActivity");
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        HiLog.info(lable, "VAL SELECTED",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean b) {
        tvX.setText(String.valueOf(seekBarX.getProgress()));
        tvY.setText(String.valueOf(seekBarY.getProgress()));

        ArrayList<Entry> values1 = new ArrayList<>();
        ArrayList<Entry> values2 = new ArrayList<>();
        ArrayList<Entry> values3 = new ArrayList<>();

        for (int i = 0; i < seekBarX.getProgress(); i++) {
            float val = (float) (Math.random() * seekBarY.getProgress()) + 3;
            values1.add(new Entry(i, val));
        }

        for (int i = 0; i < seekBarX.getProgress(); i++) {
            float val = (float) (Math.random() * seekBarY.getProgress()) + 3;
            values2.add(new Entry(i+0.33f, val));
        }

        for (int i = 0; i < seekBarX.getProgress(); i++) {
            float val = (float) (Math.random() * seekBarY.getProgress()) + 3;
            values3.add(new Entry(i+0.66f, val));
        }

        // create a dataset and give it a type
        ScatterDataSet set1 = new ScatterDataSet(values1, "DS 1");
        set1.setScatterShape(ScatterChart.ScatterShape.SQUARE);
        set1.setColor(ColorTemplate.COLORFUL_COLORS[0]);
        ScatterDataSet set2 = new ScatterDataSet(values2, "DS 2");
        set2.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
        set2.setScatterShapeHoleColor(ColorTemplate.COLORFUL_COLORS[3]);
        set2.setScatterShapeHoleRadius(3f);
        set2.setColor(ColorTemplate.COLORFUL_COLORS[1]);
        ScatterDataSet set3 = new ScatterDataSet(values3, "DS 3");
        set3.setShapeRenderer(new CustomScatterShapeRenderer());
        set3.setColor(ColorTemplate.COLORFUL_COLORS[2]);

        set1.setScatterShapeSize(8f);
        set2.setScatterShapeSize(8f);
        set3.setScatterShapeSize(8f);

        ArrayList<IScatterDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1); // add the data sets
        dataSets.add(set2);
        dataSets.add(set3);

        // create a data object with the data sets
        ScatterData data = new ScatterData(dataSets);
//        data.setValueTypeface(tfLight);

        chart.setData(data);
        chart.invalidate();
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }
}
