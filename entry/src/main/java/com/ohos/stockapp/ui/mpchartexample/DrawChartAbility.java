package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.listener.OnDrawListener;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.IDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.ArrayList;
import java.util.List;

public class DrawChartAbility extends DemoBase implements OnChartValueSelectedListener, OnDrawListener {
    private LineChart chart;

    HiLogLabel lable=new HiLogLabel(HiLog.INFO,0X367235,Chart.LOG_TAG);
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_draw_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("DrawChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("draw"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "actionToggleValues":
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setDrawValues(!set.isDrawValuesEnabled());
                                }

                                chart.invalidate();
                                break;

                            case "actionToggleHighlight":
                                if(chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;

                            case "actionTogglePinch":
                                if (chart.isPinchZoomEnabled())
                                    chart.setPinchZoom(false);
                                else
                                    chart.setPinchZoom(true);

                                chart.invalidate();
                                break;


                            case "actionToggleAutoScaleMinMax": {
                                chart.setAutoScaleMinMaxEnabled(!chart.isAutoScaleMinMaxEnabled());
                                chart.notifyDataSetChanged();
                                break;
                            }
                            case "actionSave": {
                                saveToGallery();
                                break;
                            }

                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        chart = (LineChart) findComponentById(ResourceTable.Id_chart1);

        // listener for selecting and drawing
        chart.setOnChartValueSelectedListener(this);
        chart.setOnDrawListener(this);

        // if disabled, drawn data sets with the finger will not be automatically
        // finished
        // chart.setAutoFinish(true);
        chart.setDrawGridBackground(false);

        // add dummy-data to the chart
        initWithDummyData();

        XAxis xl = chart.getXAxis();
//        xl.setTypeface(tfRegular);
        xl.setAvoidFirstLastClipping(true);

        YAxis yl = chart.getAxisLeft();
//        yl.setTypeface(tfRegular);

        chart.getLegend().setEnabled(false);

        // chart.setYRange(-40f, 40f, true);
        // call this to reset the changed y-range
        // chart.resetYRange(true);
    }

    private void initWithDummyData() {

        ArrayList<Entry> values = new ArrayList<>();

        // create a dataset and give it a type (0)
        LineDataSet set1 = new LineDataSet(values, "DataSet");
        set1.setLineWidth(3f);
        set1.setCircleRadius(5f);

        // create a data object with the data sets
        LineData data = new LineData(set1);

        chart.setData(data);
    }
    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "DrawChartActivity");
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {
        HiLog.info(lable,"",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onEntryAdded(Entry entry) {
        HiLog.info(lable, entry.toString());
    }

    @Override
    public void onEntryMoved(Entry entry) {
        HiLog.info(lable,"Point moved " + entry.toString());
    }

    @Override
    public void onDrawFinished(DataSet<?> dataSet) {
        HiLog.info(lable, "DataSet drawn. " + dataSet.toSimpleString());

        // prepare the legend again
        chart.getLegendRenderer().computeLegend(chart.getData());
    }
}
