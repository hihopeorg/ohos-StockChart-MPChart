package com.ohos.stockapp.ui.mpchartexample.notimportant;


import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.*;
import com.ohos.stockapp.ui.mpchartexample.fragments.SimpleChartDemo;
import com.github.mikephil.charting.utils.Utils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;

public class MPMainAbility extends Ability implements ListContainer.ItemClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//      getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setUIContent(ResourceTable.Layout_ability_mpmain);
//      setTitle("MPohosChart Example");
//      initialize the utilities
        Utils.init(this);

        ArrayList<ContentItem> objects = new ArrayList<>();

        ////
        objects.add(0, new ContentItem("Line Charts"));

        objects.add(1, new ContentItem("Basic", "Simple line chart."));
        objects.add(2, new ContentItem("Multiple", "Show multiple data sets."));
        objects.add(3, new ContentItem("Dual Axis", "Line chart with dual y-axes."));
        objects.add(4, new ContentItem("Inverted Axis", "Inverted y-axis."));
        objects.add(5, new ContentItem("Cubic", "Line chart with a cubic line shape."));
        objects.add(6, new ContentItem("Colorful", "Colorful line chart."));
        objects.add(7, new ContentItem("Performance", "Render 30.000 data points smoothly."));
        objects.add(8, new ContentItem("Filled", "Colored area between two lines."));

        ////
        objects.add(9, new ContentItem("Bar Charts"));

        objects.add(10, new ContentItem("Basic", "Simple bar chart."));
        objects.add(11, new ContentItem("Basic 2", "Variation of the simple bar chart."));
        objects.add(12, new ContentItem("Multiple", "Show multiple data sets."));
        objects.add(13, new ContentItem("Horizontal", "Render bar chart horizontally."));
        objects.add(14, new ContentItem("Stacked", "Stacked bar chart."));
        objects.add(15, new ContentItem("Negative", "Positive and negative values with unique colors."));
        objects.add(16, new ContentItem("Stacked 2", "Stacked bar chart with negative values."));
        objects.add(17, new ContentItem("Sine", "Sine function in bar chart format."));

        ////
        objects.add(18, new ContentItem("Pie Charts"));

        objects.add(19, new ContentItem("Basic", "Simple pie chart."));
        objects.add(20, new ContentItem("Value Lines", "Stylish lines drawn outward from slices."));
        objects.add(21, new ContentItem("Half Pie", "180° (half) pie chart."));

        ////
        objects.add(22, new ContentItem("Other Charts"));

        objects.add(23, new ContentItem("Combined Chart", "Bar and line chart together."));
        objects.add(24, new ContentItem("Scatter Plot", "Simple scatter plot."));
        objects.add(25, new ContentItem("Bubble Chart", "Simple bubble chart."));
        objects.add(26, new ContentItem("Candlestick", "Simple financial chart."));
        objects.add(27, new ContentItem("Radar Chart", "Simple web chart."));

        ////
        objects.add(28, new ContentItem("Scrolling Charts"));

        objects.add(29, new ContentItem("Multiple", "Various types of charts as fragments."));
        objects.add(30, new ContentItem("View Pager", "Swipe through different charts."));
        objects.add(31, new ContentItem("Tall Bar Chart", "Bars bigger than your screen!"));
        objects.add(32, new ContentItem("Many Bar Charts", "More bars than your screen can handle!"));

        ////
        objects.add(33, new ContentItem("Even More Line Charts"));

        objects.add(34, new ContentItem("Dynamic", "Build a line chart by adding points and sets."));
        objects.add(35, new ContentItem("Realtime", "Add data points in realtime."));
        objects.add(36, new ContentItem("Hourly", "Uses the current time to add a data point for each hour."));
        //objects.add(37, new ContentItem("Realm.io Examples", "See more examples that use Realm.io mobile database."));

        MyAdapter adapter = new MyAdapter(this, objects);

        ListContainer lv = (ListContainer) findComponentById(ResourceTable.Id_listView1);
        lv.setItemProvider(adapter);

        lv.setItemClickedListener(this);
    }

    @Override
    public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {


        Intent intent = new Intent();
        Operation operation;

        switch (i) {
            case 0:
            case 9:
            case 18:
            case 22:
            case 28:
            case 33:
                operation=null;
                new ToastDialog(this).setText("当前点击无效").show();
                break;
            case 1:
                 operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(LineChartAbility1.class.getName())
                        .build();
                break;
            case 2:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(MultiLineChartAbility.class.getName())
                        .build();
                break;
            case 3:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(LineChartAbility2.class.getName())
                        .build();
                break;
            case 4:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(InvertedLineChartAbility.class.getName())
                        .build();
                break;
            case 5:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(CubicLineChartAbility.class.getName())
                        .build();
                break;
            case 6:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(LineChartAbilityColored.class.getName())
                        .build();
                break;
            case 7:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(PerformanceLineChart.class.getName())
                        .build();
                break;
            case 8:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(FilledLineAbility.class.getName())
                        .build();
                break;
            case 10:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(BarChartAbility.class.getName())
                        .build();
                break;
            case 11:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(AnotherBarAbility.class.getName())
                        .build();
                break;
            case 12:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(BarChartAbilityMultiDataset.class.getName())
                        .build();
                break;
            case 13:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(HorizontalBarChartAbility.class.getName())
                        .build();
                break;
            case 14:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(StackedBarAbility.class.getName())
                        .build();
                break;
            case 15:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(BarChartPositiveNegative.class.getName())
                        .build();
                break;
            case 16:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(StackedBarAbilityNegative.class.getName())
                        .build();
                break;
            case 17:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(BarChartAbilitySinus.class.getName())
                        .build();
                break;
            case 19:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(PieChartAbility.class.getName())
                        .build();
                break;
            case 20:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(PiePolylineChartAbility.class.getName())
                        .build();
                break;
            case 21:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(HalfPieChartAbility.class.getName())
                        .build();
                break;
            case 23:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(CombinedChartAbility.class.getName())
                        .build();
                break;
            case 24:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(ScatterChartAbility.class.getName())
                        .build();
                break;
            case 25:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(BubbleChartAbility.class.getName())
                        .build();
                break;
            case 26:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(CandleStickChartAbility.class.getName())
                        .build();
                break;
            case 27:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(RadarChartAbility.class.getName())
                        .build();
                break;
            case 29:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(ListViewMultiChartAbility.class.getName())
                        .build();
                break;
            case 30:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(SimpleChartDemo.class.getName())
                        .build();
                break;
            case 31:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(ScrollViewAbility.class.getName())
                        .build();
                break;
            case 32:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(ListViewBarChartAbility.class.getName())
                        .build();
                break;
            case 34:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(DynamicalAddingAbility.class.getName())
                        .build();
                break;
            case 35:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(RealtimeLineChartAbility.class.getName())
                        .build();
                break;
            case 36:
                operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName("com.ohos.stockapp")
                        .withAbilityName(LineChartTime.class.getName())
                        .build();
                break;
            /*case 37:
                i = new Intent(this, RealmMainActivity.class);
                break;*/
            default:
                throw new IllegalStateException("Unexpected value: " + i);
        }
        if (intent != null&&operation!=null){
            intent.setOperation(operation);
            startAbility(intent);}

        setTransitionAnimation(ResourceTable.Animation_move_right_in_activity, ResourceTable.Animation_move_left_out_activity);
    }
}
