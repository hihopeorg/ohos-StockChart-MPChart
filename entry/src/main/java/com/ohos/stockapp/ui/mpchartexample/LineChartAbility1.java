package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.MyMarkerView;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.PathEffect;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.dialog.IDialog;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LineChartAbility1 extends DemoBase implements Slider.ValueChangedListener, OnChartValueSelectedListener {
    private LineChart chart;
    private Slider seekBarX, seekBarY;
    private Text tvX, tvY;
    private HiLogLabel lable = new HiLogLabel(HiLog.INFO, 0x647344, Chart.LOG_TAG);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_line_chart1);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("LineChartActivity1");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("line"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);
                                break;
                            case "actionToggleValues": {
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setDrawValues(!set.isDrawValuesEnabled());
                                }

                                chart.invalidate();
                                break;
                            }

                            case "actionToggleHighlight": {
                                if(chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;
                            }
                            case "actionToggleFilled": {
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    if (set.isDrawFilledEnabled())
                                        set.setDrawFilled(false);
                                    else
                                        set.setDrawFilled(true);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleCircles":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    if (set.isDrawCirclesEnabled())
                                        set.setDrawCircles(false);
                                    else
                                        set.setDrawCircles(true);
                                }
                                chart.invalidate();
                                break;}
                            case "actionToggleCubic":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                                            ? LineDataSet.Mode.LINEAR
                                            :  LineDataSet.Mode.CUBIC_BEZIER);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleStepped":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setMode(set.getMode() == LineDataSet.Mode.STEPPED
                                            ? LineDataSet.Mode.LINEAR
                                            :  LineDataSet.Mode.STEPPED);
                                }
                                chart.invalidate();
                                break;}
                            case "actionToggleHorizontalCubic":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setMode(set.getMode() == LineDataSet.Mode.HORIZONTAL_BEZIER
                                            ? LineDataSet.Mode.LINEAR
                                            :  LineDataSet.Mode.HORIZONTAL_BEZIER);
                                }
                                chart.invalidate();
                                break;}
                            case "actionTogglePinch":
                                if (chart.isPinchZoomEnabled())
                                    chart.setPinchZoom(false);
                                else
                                    chart.setPinchZoom(true);

                                chart.invalidate();
                                break;
                            case "actionToggleAutoScaleMinMax":
                                chart.setAutoScaleMinMaxEnabled(!chart.isAutoScaleMinMaxEnabled());
                                chart.notifyDataSetChanged();
                                break;
                            case "animateX":
                                chart.animateX(2000);
                                break;
                            case "animateY":
                                chart.animateY(2000);
                                break;
                            case "animateXY":
                                chart.animateXY(2000, 2000);
                                break;

                            case "actionSave":
                                saveToGallery();
                                break;

                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        tvY = (Text) findComponentById(ResourceTable.Id_tvYMax);

        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarX.setValueChangedListener(this);

        seekBarY = (Slider) findComponentById(ResourceTable.Id_seekBar2);
        seekBarY.setMaxValue(180);
        seekBarY.setValueChangedListener(this);


        {   // // Chart Style // //
            chart = (LineChart) findComponentById(ResourceTable.Id_chart1);
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(new RgbColor(Color.WHITE.getValue()));
            // background color
            chart.setBackground(shapeElement);

            // disable description text
            chart.getDescription().setEnabled(false);

            // enable touch gestures
            chart.setTouchEnabled(true);

            // set listeners
            chart.setOnChartValueSelectedListener(this);
            chart.setDrawGridBackground(false);

            // create marker to display box when values are selected
            MyMarkerView mv = new MyMarkerView(this, ResourceTable.Layout_custom_marker_view);

            // Set the marker to the chart
            mv.setChartView(chart);
            chart.setMarker(mv);

            // enable scaling and dragging
            chart.setDragEnabled(true);
            chart.setScaleEnabled(true);
            // chart.setScaleXEnabled(true);
            // chart.setScaleYEnabled(true);

            // force pinch zoom along both axis
            chart.setPinchZoom(true);
        }

        XAxis xAxis;
        {   // // X-Axis Style // //
            xAxis = chart.getXAxis();

            // vertical grid lines
            xAxis.enableGridDashedLine(10f, 10f, 0f);
        }

        YAxis yAxis;
        {   // // Y-Axis Style // //
            yAxis = chart.getAxisLeft();

            // disable dual axis (only use LEFT axis)
            chart.getAxisRight().setEnabled(false);

            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f);

            // axis range
            yAxis.setAxisMaximum(200f);
            yAxis.setAxisMinimum(-50f);
        }


        {   // // Create Limit Lines // //
            LimitLine llXAxis = new LimitLine(9f, "Index 10");
            llXAxis.setLineWidth(4f);
            llXAxis.enableDashedLine(10f, 10f, 0f);
            llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            llXAxis.setTextSize(10f);
//            llXAxis.setTypeface(tfRegular);

            LimitLine ll1 = new LimitLine(150f, "Upper Limit");
            ll1.setLineWidth(4f);
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
//            ll1.setTypeface(tfRegular);

            LimitLine ll2 = new LimitLine(-30f, "Lower Limit");
            ll2.setLineWidth(4f);
            ll2.enableDashedLine(10f, 10f, 0f);
            ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
            ll2.setTextSize(10f);
//            ll2.setTypeface(tfRegular);

            // draw limit lines behind data instead of on top
            yAxis.setDrawLimitLinesBehindData(true);
            xAxis.setDrawLimitLinesBehindData(true);

            // add limit lines
            yAxis.addLimitLine(ll1);
            yAxis.addLimitLine(ll2);
            //xAxis.addLimitLine(llXAxis);
        }

        // add data
        seekBarX.setProgressValue(45);
        seekBarY.setProgressValue(180);
        setData(45, 180);

        // draw points over time
        chart.animateX(2000);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();

        // draw legend entries as lines
        l.setForm(Legend.LegendForm.LINE);
    }

    private void setData(int count, float range) {

        ArrayList<Entry> values = new ArrayList<>();

        for (int i = 0; i < count; i++) {

            float val = (float) (Math.random() * range) - 30;
            Resource resource = null;
            try {
                resource = getResourceManager().getResource(ResourceTable.Media_star);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            values.add(new Entry(i, val, new PixelMapElement(resource)));
        }

        LineDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            set1.notifyDataSetChanged();
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "DataSet 1");

            set1.setDrawIcons(false);

            // draw dashed line
            set1.enableDashedLine(10f, 5f, 0f);

            // black lines and points
            set1.setColor(Color.BLACK.getValue());
            set1.setCircleColor(Color.BLACK.getValue());

            // line thickness and point size
            set1.setLineWidth(1f);
            set1.setCircleRadius(3f);

            // draw points as solid circles
            set1.setDrawCircleHole(false);

            // customize legend entry
            set1.setFormLineWidth(1f);
            set1.setFormLineDashEffect(new PathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            // text size of values
            set1.setValueTextSize(9f);

            // draw selection line as dashed
            set1.enableDashedHighlightLine(10f, 5f, 0f);

            // set the filled area
            set1.setDrawFilled(true);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return chart.getAxisLeft().getAxisMinimum();
                }
            });
            ShapeElement shapeElement=new ShapeElement();
            shapeElement.setShaderType(Paint.ShaderType.LINEAR_SHADER.value());
            shapeElement.setShape(ShapeElement.RECTANGLE);
            shapeElement.setRgbColors(new RgbColor[]{new RgbColor(Color.BLUE.getValue()), new RgbColor(Color.TRANSPARENT.getValue())});
            shapeElement.setGradientOrientation(ShapeElement.Orientation.TOP_TO_BOTTOM);
            set1.setFillDrawable(shapeElement);


            ArrayList<ILineDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1); // add the data sets

            // create a data object with the data sets
            LineData data = new LineData(dataSets);

            // set data
            chart.setData(data);
        }

    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "LineChartActivity1");
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        HiLog.info(lable, "Entry selected", e.toString());
        HiLog.info(lable, "LOW HIGH", "low: " + chart.getLowestVisibleX() + ", high: " + chart.getHighestVisibleX());
        HiLog.info(lable, "MIN MAX", "xMin: " + chart.getXChartMin() + ", xMax: " + chart.getXChartMax() + ", yMin: " + chart.getYChartMin() + ", yMax: " + chart.getYChartMax());
    }

    @Override
    public void onNothingSelected() {
        HiLog.info(lable, "Nothing selected", "Nothing selected.");
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean b) {
        tvX.setText(String.valueOf(seekBarX.getProgress()));
        tvY.setText(String.valueOf(seekBarY.getProgress()));

        setData(seekBarX.getProgress(), seekBarY.getProgress());

        // redraw
        chart.invalidate();
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }
}
