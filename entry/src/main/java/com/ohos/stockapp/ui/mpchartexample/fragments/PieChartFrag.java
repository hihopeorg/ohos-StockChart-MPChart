package com.ohos.stockapp.ui.mpchartexample.fragments;

import com.ohos.stockapp.ResourceTable;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class PieChartFrag extends SimpleFragment {

    public static Fraction newInstance() {
        return new PieChartFrag();
    }
    private PieChart chart;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
       Component component=scatter.parse(ResourceTable.Layout_frag_simple_pie,container,false);
        chart = (PieChart) component.findComponentById(ResourceTable.Id_pieChart1);
        chart.getDescription().setEnabled(false);

      //todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

//        chart.setCenterTextTypeface(tf);
//        chart.setCenterText(generateCenterText());
        chart.setCenterTextSize(10f);
//        chart.setCenterTextTypeface(tf);

        // radius of the center hole in percent of maximum radius
        chart.setHoleRadius(45f);
        chart.setTransparentCircleRadius(50f);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);

        chart.setData(generatePieData());

        return component;


    }
//    private SpannableString generateCenterText() {
//        SpannableString s = new SpannableString("Revenues\nQuarters 2015");
//        s.setSpan(new RelativeSizeSpan(2f), 0, 8, 0);
//        s.setSpan(new ForegroundColorSpan(Color.GRAY), 8, s.length(), 0);
//        return s;
//    }
}
