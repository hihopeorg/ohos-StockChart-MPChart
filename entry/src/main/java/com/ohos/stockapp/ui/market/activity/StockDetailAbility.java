package com.ohos.stockapp.ui.market.activity;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.market.fragment.ChartFiveDayFragment;
import com.ohos.stockapp.ui.market.fragment.ChartKLineFragment;
import com.ohos.stockapp.ui.market.fragment.ChartOneDayFragment;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.TabList;
import ohos.agp.components.Text;

public class StockDetailAbility extends FractionAbility {
    FractionScheduler fractionScheduler;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_stock_detail);
        TabList tabList = (TabList) findComponentById(ResourceTable.Id_tab);
        Text text= (Text) findComponentById(ResourceTable.Id_shuo);
        text.setText("说明：\n1，支持缩放，滑动，长按出现高亮十字线。\n" +
                "2，单击竖屏图表可进入横屏页面。\n" +
                "3，K线副图可以单击切换不同指标。\n" +
                "4，支持夜间模式。");
        TabList.Tab tab1 = tabList.new Tab(getContext());
        tab1.setText("分时");
        TabList.Tab tab2 = tabList.new Tab(getContext());
        tab2.setText("五日");
        TabList.Tab tab3 = tabList.new Tab(getContext());
        tab3.setText("日K");
        TabList.Tab tab4 = tabList.new Tab(getContext());
        tab4.setText("周K");
        TabList.Tab tab5 = tabList.new Tab(getContext());
        tab5.setText("月K");
        tabList.addTab(tab1);
        tabList.addTab(tab2);
        tabList.addTab(tab3);
        tabList.addTab(tab4);
        tabList.addTab(tab5);
        fractionScheduler=getFractionManager()
                .startFractionScheduler();
        fractionScheduler.add(ResourceTable.Id_view_pager, ChartOneDayFragment.newInstance(false)).submit();
        tab1.select();
        tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
            @Override
            public void onSelected(TabList.Tab tab) {
                startFraction(tab.getPosition());
            }

            @Override
            public void onUnselected(TabList.Tab tab) {

            }

            @Override
            public void onReselected(TabList.Tab tab) {

            }
        });
    }
    public void startFraction(int  code){
        switch (code){
            case 0:
                getFractionManager().startFractionScheduler().replace(ResourceTable.Id_view_pager,ChartOneDayFragment.newInstance(false)).submit();
                break;
            case 1:
                getFractionManager().startFractionScheduler().replace(ResourceTable.Id_view_pager, ChartFiveDayFragment.newInstance(false)).submit();
                break;
            case 2:
                getFractionManager().startFractionScheduler().replace(ResourceTable.Id_view_pager, ChartKLineFragment.newInstance(1, false)).submit();
                break;
            case 3:
                getFractionManager().startFractionScheduler().replace(ResourceTable.Id_view_pager,ChartKLineFragment.newInstance(7, false)).submit();
                break;
            case 4:
               getFractionManager().startFractionScheduler().replace(ResourceTable.Id_view_pager,ChartKLineFragment.newInstance(30, false)).submit();
                break;
        }
    }
}
