package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.utils.net.Uri;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class LineChartTime extends DemoBase implements Slider.ValueChangedListener {
    private LineChart chart;
    private Slider seekBarX;
    private Text tvX;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setUIContent(ResourceTable.Layout_ability_line_chart_time);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("LineChartTime");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("line"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);
                                break;
                            case "actionToggleValues": {
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setDrawValues(!set.isDrawValuesEnabled());
                                }

                                chart.invalidate();
                                break;
                            }

                            case "actionToggleHighlight": {
                                if(chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;
                            }
                            case "actionToggleFilled": {
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    if (set.isDrawFilledEnabled())
                                        set.setDrawFilled(false);
                                    else
                                        set.setDrawFilled(true);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleCircles":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    if (set.isDrawCirclesEnabled())
                                        set.setDrawCircles(false);
                                    else
                                        set.setDrawCircles(true);
                                }
                                chart.invalidate();
                                break;}
                            case "actionToggleCubic":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {
                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setMode(set.getMode() == LineDataSet.Mode.CUBIC_BEZIER
                                            ? LineDataSet.Mode.LINEAR
                                            :  LineDataSet.Mode.CUBIC_BEZIER);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleStepped":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setMode(set.getMode() == LineDataSet.Mode.STEPPED
                                            ? LineDataSet.Mode.LINEAR
                                            :  LineDataSet.Mode.STEPPED);
                                }
                                chart.invalidate();
                                break;}
                            case "actionToggleHorizontalCubic":{
                                List<ILineDataSet> sets = chart.getData()
                                        .getDataSets();

                                for (ILineDataSet iSet : sets) {

                                    LineDataSet set = (LineDataSet) iSet;
                                    set.setMode(set.getMode() == LineDataSet.Mode.HORIZONTAL_BEZIER
                                            ? LineDataSet.Mode.LINEAR
                                            :  LineDataSet.Mode.HORIZONTAL_BEZIER);
                                }
                                chart.invalidate();
                                break;}
                            case "actionTogglePinch":
                                if (chart.isPinchZoomEnabled())
                                    chart.setPinchZoom(false);
                                else
                                    chart.setPinchZoom(true);

                                chart.invalidate();
                                break;
                            case "actionToggleAutoScaleMinMax":
                                chart.setAutoScaleMinMaxEnabled(!chart.isAutoScaleMinMaxEnabled());
                                chart.notifyDataSetChanged();
                                break;
                            case "animateX":
                                chart.animateX(2000);
                                break;
                            case "animateY":
                                chart.animateY(2000);
                                break;
                            case "animateXY":
                                chart.animateXY(2000, 2000);
                                break;

                            case "actionSave":
                                saveToGallery();
                                break;

                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarX.setValueChangedListener(this);

        chart = (LineChart) findComponentById(ResourceTable.Id_chart1);

        // no description text
        chart.getDescription().setEnabled(false);

        // enable touch gestures
        chart.setTouchEnabled(true);

        chart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);
        chart.setHighlightPerDragEnabled(true);

        // set an alternative background color
        ShapeElement shapeElement=new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(Color.WHITE.getValue()));
        chart.setBackground(shapeElement);
        chart.setViewPortOffsets(0f, 0f, 0f, 0f);

        // add data
        seekBarX.setProgressValue(100);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.TOP_INSIDE);
//        xAxis.setTypeface(tfLight);
        xAxis.setTextSize(10f);
        xAxis.setTextColor(Color.WHITE.getValue());
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(true);
        xAxis.setTextColor(Color.rgb(255, 192, 56));
        xAxis.setCenterAxisLabels(true);
        xAxis.setGranularity(1f); // one hour
        xAxis.setValueFormatter(new ValueFormatter() {
            private final SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM HH:mm", Locale.ENGLISH);

            @Override
            public String getFormattedValue(float value) {

                long millis = TimeUnit.HOURS.toMillis((long) value);
                return mFormat.format(new Date(millis));
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
//        leftAxis.setTypeface(tfLight);
        leftAxis.setTextColor(ColorTemplate.getHoloBlue());
        leftAxis.setDrawGridLines(true);
        leftAxis.setGranularityEnabled(true);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(170f);
        leftAxis.setYOffset(-9f);
        leftAxis.setTextColor(Color.rgb(255, 192, 56));
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }
    private void setData(int count, float range) {

        // now in hours
        long now = TimeUnit.MILLISECONDS.toHours(System.currentTimeMillis());

        ArrayList<Entry> values = new ArrayList<>();

        // count = hours
        float to = now + count;

        // increment by 1 hour
        for (float x = now; x < to; x++) {
            float y = getRandom(range, 50);
            values.add(new Entry(x, y)); // add one entry per hour
        }

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(values, "DataSet 1");
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setColor(ColorTemplate.getHoloBlue());
        set1.setValueTextColor(ColorTemplate.getHoloBlue());
        set1.setLineWidth(1.5f);
        set1.setDrawCircles(false);
        set1.setDrawValues(false);
        set1.setFillAlpha(65);
        set1.setFillColor(ColorTemplate.getHoloBlue());
        set1.setHighLightColor(Color.rgb(244, 117, 117));
        set1.setDrawCircleHole(false);
        // create a data object with the data sets
        LineData data = new LineData(set1);
        data.setValueTextColor(Color.WHITE.getValue());
        data.setValueTextSize(9f);
        // set data
        chart.setData(data);
    }
    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "LineChartTime");
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean b) {
        tvX.setText(String.valueOf(seekBarX.getProgress()));

        setData(seekBarX.getProgress(), 50);

        // redraw
        chart.invalidate();
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }
}
