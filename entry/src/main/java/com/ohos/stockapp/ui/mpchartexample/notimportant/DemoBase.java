
package com.ohos.stockapp.ui.mpchartexample.notimportant;


import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.common.data.MenuBean;
import com.github.mikephil.charting.charts.Chart;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * Base class of all Activities of the Demo Application.
 *
 * @author Philipp Jahoda
 */
public abstract class DemoBase extends FractionAbility {
    public String app_github_url="https://github.com/PhilJay/MPohosChart/blob/master/MPChartExample/src/com/xxmassdeveloper/mpchartexample/AnotherBarActivity.java";
    protected final String[] months = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"
    };

    protected final String[] parties = new String[]{
            "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
    };

    private static final int PERMISSION_STORAGE = 0;

//todo   protected Typeface tfRegular;
//todo    protected Typeface tfLight;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
//        tfRegular = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
//        tfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");
    }

    public List<MenuBean> getList(String type) {
        List<MenuBean> list = new ArrayList<>();
        switch (type) {
            case "bar":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleBarBorders","Toggle Bar Borders"));
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("actionTogglePinch","Toggle PinchZoom"));
                list.add(new MenuBean("actionToggleAutoScaleMinMax","Toggle Auto Scale"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "bubble":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("actionTogglePinch","Toggle PinchZoom"));
                list.add(new MenuBean("actionToggleAutoScaleMinMax","Toggle Auto Scale"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "candle":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("actionToggleMakeShadowSameColorAsCandle","Toggle Shadow Color"));
                list.add(new MenuBean("actionTogglePinch","Toggle PinchZoom"));
                list.add(new MenuBean("actionToggleAutoScaleMinMax","Toggle Auto Scale"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "combined":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleLineValues","Toggle Line Values"));
                list.add(new MenuBean("actionToggleBarValues","Toggle Bar Values"));
                list.add(new MenuBean("actionRemoveDataSet","Remove Data Set"));
                break;
            case "draw":
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleFilled","Toggle Filled"));
                list.add(new MenuBean("actionToggleCircles","Toggle Circles"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("actionTogglePinch","Toggle PinchZoom"));
                list.add(new MenuBean("actionToggleAutoScaleMinMax","Toggle Auto Scale"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "dynamical":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionAddEntry","Add Entry"));
                list.add(new MenuBean("actionRemoveEntry","Remove Entry"));
                list.add(new MenuBean("actionAddDataSet","Add Data Set"));
                list.add(new MenuBean("actionRemoveDataSet","Remove Data Set"));
                list.add(new MenuBean("actionClear","Clear chart"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "line":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionToggleFilled","Toggle Filled"));
                list.add(new MenuBean("actionToggleCircles","Toggle Circles"));
                list.add(new MenuBean("actionToggleCubic","Toggle Cubic"));
                list.add(new MenuBean("actionToggleStepped","Toggle Stepped"));
                list.add(new MenuBean("actionToggleHorizontalCubic","Toggle Horizontal Cubic"));
                list.add(new MenuBean("actionTogglePinch","Toggle PinchZoom"));
                list.add(new MenuBean("actionToggleAutoScaleMinMax","Toggle Auto Scale"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "main":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("report","Problem Report"));
                list.add(new MenuBean("website","Developer Website"));
                break;
            case "menu_right":
                list.add(new MenuBean("item_model","日夜间模式"));
                break;
            case "only_github":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                break;
            case "pie":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleValues","Toggle Y-Values"));
                list.add(new MenuBean("actionToggleXValues","Toggle X-Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionTogglePercent","Toggle Percent"));
                list.add(new MenuBean("actionToggleMinAngles","Toggle Minimum Angles"));
                list.add(new MenuBean("actionToggleHole","Toggle Hole"));
                list.add(new MenuBean("actionToggleCurvedSlices","Toggle Curved Slices"));
                list.add(new MenuBean("actionDrawCenter","Draw Center Text"));
                list.add(new MenuBean("actionToggleSpin","Spin Animation"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "radar":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionToggleFilled","Toggle Filled"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("actionToggleHighlightCircle","Toggle Horizontal Circle"));
                list.add(new MenuBean("actionToggleRotate","Toggle Rotation"));
                list.add(new MenuBean("actionToggleYLabels","Toggle Y-Values"));
                list.add(new MenuBean("actionToggleXLabels","Toggle X-Values"));
                list.add(new MenuBean("actionToggleSpin","Spin Animation"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "realtime":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionAdd","Add Entry"));
                list.add(new MenuBean("actionClear","Clear Entry"));
                list.add(new MenuBean("actionFeedMultiple","Add Multiple"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
            case "scatter":
                list.add(new MenuBean("viewGithub","View on GitHub"));
                list.add(new MenuBean("actionToggleValues","Toggle Values"));
                list.add(new MenuBean("actionToggleIcons","Toggle Icons"));
                list.add(new MenuBean("actionToggleHighlight","Toggle Highlight"));
                list.add(new MenuBean("animateX","Animate X"));
                list.add(new MenuBean("animateY","Animate Y"));
                list.add(new MenuBean("animateXY","Animate XY"));
                list.add(new MenuBean("actionTogglePinch","Toggle PinchZoom"));
                list.add(new MenuBean("actionToggleAutoScaleMinMax","Toggle Auto Scale"));
                list.add(new MenuBean("actionSave","Save to Gallery"));
                break;
        }
        return list;
    }


    protected float getRandom(float range, float start) {
        return (float) (Math.random() * range) + start;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setTransitionAnimation(ResourceTable.Animation_move_left_in_activity, ResourceTable.Animation_move_right_out_activity);
    }


    protected void saveToGallery(Chart chart, String name) {
        if (chart.saveToGallery(name + "_" + System.currentTimeMillis(), 70))
            new ToastDialog(getContext()).setText("Saving SUCCESSFUL!").show();
        else
            new ToastDialog(getContext()).setText("Saving FAILED!").show();
    }

    protected abstract void saveToGallery();
}
