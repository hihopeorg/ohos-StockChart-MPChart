package com.ohos.stockapp.ui.market.dialog;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.common.data.MenuBean;
import ohos.agp.components.*;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_CONTENT;

public class MyDialog extends CommonDialog {
    private MyDialogListener confirmListener;
    private Context context;
    private List<MenuBean> listItems=new ArrayList<>();

    public MyDialog(Context context, List<MenuBean> items,MyDialogListener dialogListener) {
        super(context);
        this.context = context;
        listItems.addAll(items);
        confirmListener=dialogListener;
        initComponents();

        setCornerRadius(AttrHelper.vp2px(10, context));
        setAlignment(TextAlignment.RIGHT);
        setSize(AttrHelper.vp2px(150, context), MATCH_CONTENT);
    }


    private void initComponents() {
        Component customComponent = LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_custom_dialog_content, null, true);
        ListContainer listContainer = (ListContainer) customComponent.findComponentById(ResourceTable.Id_list);
        listContainer.setItemProvider(new ListAdapter());
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                if(confirmListener!=null)
                    confirmListener.DialogItemListener(listItems.get(i).getId(),MyDialog.this);
            }
        });
        super.setContentCustomComponent(customComponent);

    }
   public class ListAdapter extends BaseItemProvider{

       @Override
       public int getCount() {
           return listItems.size();
       }

       @Override
       public Object getItem(int i) {
           return listItems.get(i);
       }

       @Override
       public long getItemId(int i) {
           return i;
       }

       @Override
       public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
           component=LayoutScatter.getInstance(context).parse(ResourceTable.Layout_dialog_item,null,false);
          Text tv = (Text) component.findComponentById(ResourceTable.Id_tv);
           tv.setText(listItems.get(i).getName());
           return component;
       }
   }

    public interface MyDialogListener {
        void DialogItemListener(String id, IDialog dialog);
    }
}
