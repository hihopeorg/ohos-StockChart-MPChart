package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.MyMarkerView;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.Locale;

public class BarChartAbilityMultiDataset extends DemoBase implements Slider.ValueChangedListener, OnChartValueSelectedListener {
    private BarChart chart;
    private Slider seekBarX, seekBarY;
    private Text tvX, tvY;
    HiLogLabel lable=new HiLogLabel(HiLog.INFO,0x456463,"Activity");
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setUIContent(ResourceTable.Layout_ability_bar_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("BarChartActivityMultiDataset");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("bar"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer=new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);

                                break;
                            case "actionToggleValues":
                                for (IBarDataSet set : chart.getData().getDataSets())
                                    set.setDrawValues(!set.isDrawValuesEnabled());

                                chart.invalidate();
                                break;
                            case "actionToggleHighlight":
                                if(chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;
                            case "actionTogglePinch":
                                if (chart.isPinchZoomEnabled())
                                    chart.setPinchZoom(false);
                                else
                                    chart.setPinchZoom(true);

                                chart.invalidate();
                                break;
                            case "actionToggleAutoScaleMinMax":
                                chart.setAutoScaleMinMaxEnabled(!chart.isAutoScaleMinMaxEnabled());
                                chart.notifyDataSetChanged();
                                break;
                            case "actionToggleBarBorders":
                                for (IBarDataSet set : chart.getData().getDataSets())
                                    ((BarDataSet)set).setBarBorderWidth(set.getBarBorderWidth() == 1.f ? 0.f : 1.f);

                                chart.invalidate();
                                break;
                            case "animateX":
                                chart.animateX(2000);
                                break;
                            case "animateY":
                                chart.animateY(2000);
                                break;
                            case "animateXY":
                                chart.animateXY(2000, 2000);
                                break;
                            case "actionSave":
                                saveToGallery();
                                break;
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });


    }

    @Override
    protected void onActive() {
        super.onActive();
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        tvX.setTextSize(10);
        tvY = (Text) findComponentById(ResourceTable.Id_tvYMax);

        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarX.setMaxValue(50);
        seekBarX.setValueChangedListener(this);

        seekBarY = (Slider) findComponentById(ResourceTable.Id_seekBar2);
        seekBarY.setValueChangedListener(this);

        chart = (BarChart) findComponentById(ResourceTable.Id_chart1);
        chart.setOnChartValueSelectedListener(this);
        chart.getDescription().setEnabled(false);

//        chart.setDrawBorders(true);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawBarShadow(false);

        chart.setDrawGridBackground(false);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv = new MyMarkerView(this, ResourceTable.Layout_custom_marker_view);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        seekBarX.setProgressValue(10);
        seekBarY.setProgressValue(100);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
//        l.setTypeface(tfLight);
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(8f);

        XAxis xAxis = chart.getXAxis();
//        xAxis.setTypeface(tfLight);
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) value);
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(tfLight);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "BarChartActivityMultiDataset");
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

        HiLog.info(lable,"Selected: " + e.toString() + ", dataSet: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {
        HiLog.info(lable, "Nothing selected.");
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
        float groupSpace = 0.08f;
        float barSpace = 0.03f; // x4 DataSet
        float barWidth = 0.2f; // x4 DataSet
        // (0.2 + 0.03) * 4 + 0.08 = 1.00 -> interval per "group"

        int groupCount = seekBarX.getProgress() + 1;
        int startYear = 1980;
        int endYear = startYear + groupCount;

        tvX.setText(String.format(Locale.ENGLISH, "%d-%d", startYear, endYear));
        tvY.setText(String.valueOf(seekBarY.getProgress()));

        ArrayList<BarEntry> values1 = new ArrayList<>();
        ArrayList<BarEntry> values2 = new ArrayList<>();
        ArrayList<BarEntry> values3 = new ArrayList<>();
        ArrayList<BarEntry> values4 = new ArrayList<>();

        float randomMultiplier = seekBarY.getProgress() * 100000f;

        for (int i = startYear; i < endYear; i++) {
            values1.add(new BarEntry(i, (float) (Math.random() * randomMultiplier)));
            values2.add(new BarEntry(i, (float) (Math.random() * randomMultiplier)));
            values3.add(new BarEntry(i, (float) (Math.random() * randomMultiplier)));
            values4.add(new BarEntry(i, (float) (Math.random() * randomMultiplier)));
        }

        BarDataSet set1, set2, set3, set4;

        if (chart.getData() != null && chart.getData().getDataSetCount() > 0) {

            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set2 = (BarDataSet) chart.getData().getDataSetByIndex(1);
            set3 = (BarDataSet) chart.getData().getDataSetByIndex(2);
            set4 = (BarDataSet) chart.getData().getDataSetByIndex(3);
            set1.setValues(values1);
            set2.setValues(values2);
            set3.setValues(values3);
            set4.setValues(values4);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();

        } else {
            // create 4 DataSets
            set1 = new BarDataSet(values1, "Company A");
            set1.setColor(Color.rgb(104, 241, 175));
            set2 = new BarDataSet(values2, "Company B");
            set2.setColor(Color.rgb(164, 228, 251));
            set3 = new BarDataSet(values3, "Company C");
            set3.setColor(Color.rgb(242, 247, 158));
            set4 = new BarDataSet(values4, "Company D");
            set4.setColor(Color.rgb(255, 102, 0));

            BarData data = new BarData(set1, set2, set3, set4);
            data.setValueFormatter(new LargeValueFormatter());
//            data.setValueTypeface(tfLight);

            chart.setData(data);
        }

        // specify the width each bar should have
        chart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        chart.getXAxis().setAxisMinimum(startYear);

        // barData.getGroupWith(...) is a helper that calculates the width each group needs based on the provided parameters
        chart.getXAxis().setAxisMaximum(startYear + chart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        chart.groupBars(startYear, groupSpace, barSpace);
        chart.invalidate();
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }
}
