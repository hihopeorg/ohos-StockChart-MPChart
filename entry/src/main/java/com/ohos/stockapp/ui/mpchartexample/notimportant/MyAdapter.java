package com.ohos.stockapp.ui.mpchartexample.notimportant;
import com.ohos.stockapp.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Philipp Jahoda on 07/12/15.
 */
class MyAdapter extends BaseItemProvider {

//    private final Typeface mTypeFaceLight;
//    private final Typeface mTypeFaceRegular;
    private  List<ContentItem> list=new ArrayList<>();
    private Context mContext;
    MyAdapter(Context context, List<ContentItem> objects) {
        super();
        mContext=context;
        list.clear();
        list.addAll(objects);
//        mTypeFaceLight = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");
//        mTypeFaceRegular = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        ContentItem c = (ContentItem) getItem(i);

        ViewHolder holder;

        holder = new ViewHolder();

        if (c != null && c.isSection) {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_list_item_section, null,true);
        } else {
            component = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_list_item, null,true);
        }

        holder.tvName = (Text) component.findComponentById(ResourceTable.Id_tvName);
        holder.tvDesc = (Text) component.findComponentById(ResourceTable.Id_tvDesc);



//        if (c != null && c.isSection)
//            holder.tvName.setTypeface(mTypeFaceRegular);
//        else
//            holder.tvName.setTypeface(mTypeFaceLight);
//        holder.tvDesc.setTypeface(mTypeFaceLight);

        holder.tvName.setText(c != null ? c.name : null);
        holder.tvDesc.setText(c != null ? c.desc : null);

        return component;
    }

    private class ViewHolder {

        Text tvName, tvDesc;
    }
}
