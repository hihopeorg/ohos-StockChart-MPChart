package com.ohos.stockapp.ui.mpchartexample.listviewitems;



import com.github.mikephil.charting.data.ChartData;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * Base class of the Chart ListView items
 * @author philipp
 *
 */
@SuppressWarnings("unused")
public abstract class ChartItem {

    static final int TYPE_BARCHART = 0;
    static final int TYPE_LINECHART = 1;
    static final int TYPE_PIECHART = 2;

    ChartData<?> mChartData;

    ChartItem(ChartData<?> cd) {
        this.mChartData = cd;
    }

    public abstract int getItemType();

    public abstract Component getView(int position, Component convertView, Context c);
}
