package com.ohos.stockapp.ui.mpchartexample.fragments;

import com.ohos.stockapp.ResourceTable;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class ComplexityFragment extends SimpleFragment {
    public static Fraction newInstance() {
        return new ComplexityFragment();
    }
    private LineChart chart;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component=scatter.parse(ResourceTable.Layout_frag_simple_line,container,false);
        chart = (LineChart) component.findComponentById(ResourceTable.Id_lineChart1);

        chart.getDescription().setEnabled(false);

        chart.setDrawGridBackground(false);

        chart.setData(getComplexity());
        chart.animateX(3000);

//todo Typeface tf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Light.ttf");

        Legend l = chart.getLegend();
//        l.setTypeface(tf);

        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(tf);

        chart.getAxisRight().setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(false);

        return component;
    }
}
