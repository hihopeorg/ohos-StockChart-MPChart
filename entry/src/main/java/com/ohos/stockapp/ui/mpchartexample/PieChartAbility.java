package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.util.ArrayList;

public class PieChartAbility extends DemoBase implements Slider.ValueChangedListener, OnChartValueSelectedListener {
    private PieChart chart;
    private Slider seekBarX, seekBarY;
    private Text tvX, tvY;
    private Image image;
    private HiLogLabel lable = new HiLogLabel(HiLog.INFO, 0x5632572, Chart.LOG_TAG);

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setUIContent(ResourceTable.Layout_ability_pie_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("PieChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("pie"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);

                                break;
                            case "actionToggleValues":
                                for (IDataSet<?> set : chart.getData().getDataSets())
                                    set.setDrawValues(!set.isDrawValuesEnabled());

                                chart.invalidate();
                                break;
                            case "actionToggleIcons": {
                                for (IDataSet<?> set : chart.getData().getDataSets())
                                    set.setDrawIcons(!set.isDrawIconsEnabled());

                                chart.invalidate();
                                break;
                            }
                            case "actionToggleHole": {
                                if (chart.isDrawHoleEnabled())
                                    chart.setDrawHoleEnabled(false);
                                else
                                    chart.setDrawHoleEnabled(true);
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleMinAngles": {
                                if (chart.getMinAngleForSlices() == 0f)
                                    chart.setMinAngleForSlices(36f);
                                else
                                    chart.setMinAngleForSlices(0f);
                                chart.notifyDataSetChanged();
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleCurvedSlices": {
                                boolean toSet = !chart.isDrawRoundedSlicesEnabled() || !chart.isDrawHoleEnabled();
                                chart.setDrawRoundedSlices(toSet);
                                if (toSet && !chart.isDrawHoleEnabled()) {
                                    chart.setDrawHoleEnabled(true);
                                }
                                if (toSet && chart.isDrawSlicesUnderHoleEnabled()) {
                                    chart.setDrawSlicesUnderHole(false);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionDrawCenter": {
                                if (chart.isDrawCenterTextEnabled())
                                    chart.setDrawCenterText(false);
                                else
                                    chart.setDrawCenterText(true);
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleXValues": {

                                chart.setDrawEntryLabels(!chart.isDrawEntryLabelsEnabled());
                                chart.invalidate();
                                break;
                            }
                            case "actionTogglePercent":
                                chart.setUsePercentValues(!chart.isUsePercentValuesEnabled());
                                chart.invalidate();
                                break;
                            case "animateX": {
                                chart.animateX(1400);
                                break;
                            }
                            case "animateY": {
                                chart.animateY(1400);
                                break;
                            }
                            case "animateXY": {
                                chart.animateXY(1400, 1400);
                                break;
                            }
                            case "actionToggleSpin": {
                                chart.spin(1000, chart.getRotationAngle(), chart.getRotationAngle() + 360, Easing.EaseInOutCubic);
                                break;
                            }
                            case "actionSave": {
                                    saveToGallery();
                                break;
                            }
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        image= (Image) findComponentById(ResourceTable.Id_img);
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        tvY = (Text) findComponentById(ResourceTable.Id_tvYMax);

        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarY = (Slider) findComponentById(ResourceTable.Id_seekBar2);

        seekBarX.setValueChangedListener(this);
        seekBarY.setValueChangedListener(this);

        chart = (PieChart) findComponentById(ResourceTable.Id_chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

//        chart.setCenterTextTypeface(tfLight);
        chart.setCenterText(generateCenterSpannableText());

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE.getValue());

        chart.setTransparentCircleColor(Color.WHITE.getValue());
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        seekBarX.setProgressValue(4);
        seekBarY.setProgressValue(10);

        chart.animateY(2000, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        chart.setEntryLabelColor(Color.WHITE.getValue());
//        chart.setEntryLabelTypeface(tfRegular);
        chart.setEntryLabelTextSize(12f);
    }

    private void setData(int count, float range) {
        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count; i++) {
            Resource resource = null;
            try {
                resource = getResourceManager().getResource(ResourceTable.Media_star);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NotExistException e) {
                e.printStackTrace();
            }
            PixelMapElement pixelMapElement=new PixelMapElement(resource);
            entries.add(new PieEntry((float) ((Math.random() * range) + range / 5),
                    parties[i % parties.length],
                    pixelMapElement));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
//        dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(chart));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE.getValue());
//        data.setValueTypeface(tfLight);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "PieChartActivity");
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean b) {
        tvX.setText(String.valueOf(seekBarX.getProgress()));
        tvY.setText(String.valueOf(seekBarY.getProgress()));
        setData(seekBarX.getProgress(), seekBarY.getProgress());
    }

    private String generateCenterSpannableText() {

        String s = "MPohosChart\ndeveloped by Philipp Jahoda";
        return s;
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
        HiLog.info(lable, "VAL SELECTED",
                "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {
        HiLog.info(lable, "PieChart", "nothing selected");
    }
}
