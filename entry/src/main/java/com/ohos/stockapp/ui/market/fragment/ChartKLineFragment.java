package com.ohos.stockapp.ui.market.fragment;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.market.activity.StockDetailAbility;
import com.ohos.stockapp.ui.base.BaseFragment;
import com.ohos.stockapp.common.data.ChartData;
import com.ohos.stockapp.ui.market.activity.StockDetailLandAbility;
import com.github.mikephil.charting.stockChart.KLineChart;
import com.github.mikephil.charting.stockChart.charts.CoupleChartGestureListener;
import com.github.mikephil.charting.stockChart.dataManage.KLineDataManage;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.utils.zson.ZSONException;
import ohos.utils.zson.ZSONObject;

public class ChartKLineFragment extends BaseFragment {
    KLineChart combinedchart;
    private  int mType;//日K：1；周K：7；月K：30
    private  boolean land;//是否横屏

    private KLineDataManage kLineData;
    private ZSONObject object;
    private int indexType = 1;
    public static ChartKLineFragment newInstance(int typ,boolean lan){
        ChartKLineFragment fragment = new ChartKLineFragment();
        fragment.mType=typ;
        fragment.land=lan;
        return fragment;

    }



    @Override
    protected int setLayoutId() {
        return ResourceTable.Layout_fragment_kline;
    }

    @Override
    protected void initBase(Component view) {
        combinedchart= (KLineChart) view.findComponentById(ResourceTable.Id_combinedchart);
        kLineData = new KLineDataManage(getFractionAbility());
        combinedchart.initChart(land);
        try {
            if(mType == 1){
                object = ZSONObject.stringToZSON(ChartData.getKLINEDATA());
            }else if(mType == 7){
                object = ZSONObject.stringToZSON(ChartData.getKLINEWEEKDATA());
            }else if(mType == 30){
                object =ZSONObject.stringToZSON(ChartData.getKLINEMONTHDATA());
            }
        } catch (ZSONException e) {
            e.printStackTrace();
        }
        //上证指数代码000001.IDX.SH
        kLineData.parseKlineData(object,"000001.IDX.SH",land);
        combinedchart.setDataToChart(kLineData);

        combinedchart.getGestureListenerCandle().setCoupleClick(new CoupleChartGestureListener.CoupleClick() {
            @Override
            public void singleClickListener() {
                if(!land) {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName("com.ohos.stockapp")
                            .withAbilityName(StockDetailLandAbility.class.getName())
                            .build();
                    intent.setOperation(operation);
                    getFractionAbility().startAbility(intent);
                }
            }
        });
        combinedchart.getGestureListenerBar().setCoupleClick(new CoupleChartGestureListener.CoupleClick() {
            @Override
            public void singleClickListener() {
                if(land) {
                    loadIndexData(indexType < 5 ? ++indexType : 1);
                }else {
                    Intent intent = new Intent();
                    Operation operation = new Intent.OperationBuilder()
                            .withDeviceId("")
                            .withBundleName("com.ohos.stockapp")
                            .withAbilityName(StockDetailLandAbility.class.getName())
                            .build();
                    intent.setOperation(operation);
                    getFractionAbility().startAbility(intent);
                }
            }
        });
    }
    private void loadIndexData(int type) {
        indexType = type;
        switch (indexType) {
            case 1://成交量
                combinedchart.doBarChartSwitch(indexType);
                break;
            case 2://请求MACD
                kLineData.initMACD();
                combinedchart.doBarChartSwitch(indexType);
                break;
            case 3://请求KDJ
                kLineData.initKDJ();
                combinedchart.doBarChartSwitch(indexType);
                break;
            case 4://请求BOLL
                kLineData.initBOLL();
                combinedchart.doBarChartSwitch(indexType);
                break;
            case 5://请求RSI
                kLineData.initRSI();
                combinedchart.doBarChartSwitch(indexType);
                break;
            default:
                break;
        }
    }
}
