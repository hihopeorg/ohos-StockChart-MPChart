package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.custom.DayAxisValueFormatter;
import com.ohos.stockapp.ui.mpchartexample.custom.MyValueFormatter;
import com.ohos.stockapp.ui.mpchartexample.custom.XYMarkerView;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.model.GradientColor;
import com.github.mikephil.charting.utils.MPPointF;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.dialog.IDialog;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BarChartAbility extends DemoBase implements Slider.ValueChangedListener, OnChartValueSelectedListener {
    private BarChart chart;
    private Slider seekBarX, seekBarY;
    private Text tvX, tvY;
    HiLogLabel lable = new HiLogLabel(HiLog.INFO, 0x5326423, "BarChartAbility");

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setUIContent(ResourceTable.Layout_ability_another_bar);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("BarChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("bar"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);

                                break;
                            case "actionToggleValues":
                                for (IDataSet set : chart.getData().getDataSets())
                                    set.setDrawValues(!set.isDrawValuesEnabled());

                                chart.invalidate();
                                break;
                            case "actionToggleIcons":
                                for (IDataSet set : chart.getData().getDataSets())
                                    set.setDrawIcons(!set.isDrawIconsEnabled());

                                chart.invalidate();
                                break;
                            case "actionToggleHighlight":
                                if (chart.getData() != null) {
                                    chart.getData().setHighlightEnabled(!chart.getData().isHighlightEnabled());
                                    chart.invalidate();
                                }
                                break;
                            case "actionTogglePinch":
                                if (chart.isPinchZoomEnabled())
                                    chart.setPinchZoom(false);
                                else
                                    chart.setPinchZoom(true);

                                chart.invalidate();
                                break;
                            case "actionToggleAutoScaleMinMax":
                                chart.setAutoScaleMinMaxEnabled(!chart.isAutoScaleMinMaxEnabled());
                                chart.notifyDataSetChanged();
                                break;
                            case "actionToggleBarBorders":
                                for (IBarDataSet set : chart.getData().getDataSets())
                                    ((BarDataSet) set).setBarBorderWidth(set.getBarBorderWidth() == 1.f ? 0.f : 1.f);

                                chart.invalidate();
                                break;
                            case "animateX":
                                chart.animateX(2000);
                                break;
                            case "animateY":
                                chart.animateY(2000);
                                break;
                            case "animateXY":
                                chart.animateXY(2000, 2000);
                                break;
                            case "actionSave":
                                saveToGallery();
                                break;
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });


    }

    @Override
    protected void onActive() {
        super.onActive();
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        tvY = (Text) findComponentById(ResourceTable.Id_tvYMax);

        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarY = (Slider) findComponentById(ResourceTable.Id_seekBar2);

        seekBarY.setValueChangedListener(this);
        seekBarX.setValueChangedListener(this);

        chart = (BarChart) findComponentById(ResourceTable.Id_chart1);
        chart.setOnChartValueSelectedListener(this);

        chart.setDrawBarShadow(false);
        chart.setDrawValueAboveBar(true);

        chart.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawGridBackground(false);
        // chart.setDrawYLabels(false);

        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(chart);

        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setTypeface(tfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(7);
        xAxis.setValueFormatter(xAxisFormatter);

        ValueFormatter custom = new MyValueFormatter("$");

        YAxis leftAxis = chart.getAxisLeft();
//        leftAxis.setTypeface(tfLight);
        leftAxis.setLabelCount(8, false);
        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setDrawGridLines(false);
//        rightAxis.setTypeface(tfLight);
        rightAxis.setLabelCount(8, false);
        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        XYMarkerView mv = new XYMarkerView(this, xAxisFormatter);
        mv.setChartView(chart); // For bounds control
        chart.setMarker(mv); // Set the marker to the chart

        // setting data
        seekBarY.setProgressValue(50);
        seekBarX.setProgressValue(12);

        // chart.setDrawLegend(false);
    }

    private void setData(int count, float range) {

        float start = 1f;

        ArrayList<BarEntry> values = new ArrayList<>();

        for (int i = (int) start; i < start + count; i++) {
            float val = (float) (Math.random() * (range + 1));

            if (Math.random() * 100 < 25) {
                Resource resource = null;
                try {
                    resource = getResourceManager().getResource(ResourceTable.Media_star);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                }
                PixelMapElement element = new PixelMapElement(resource);
                values.add(new BarEntry(i, val, element));
            } else {
                values.add(new BarEntry(i, val));
            }
        }

        BarDataSet set1;

        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();

        } else {
            set1 = new BarDataSet(values, "The year 2017");

            set1.setDrawIcons(false);




            int startColor1 = getColor(ResourceTable.Color_holo_orange_light);
            int startColor2 = getColor(ResourceTable.Color_holo_blue_light);
            int startColor3 = getColor(ResourceTable.Color_holo_orange_light);
            int startColor4 = getColor(ResourceTable.Color_holo_green_light);
            int startColor5 = getColor(ResourceTable.Color_holo_red_light);
            int endColor1 = getColor(ResourceTable.Color_holo_blue_dark);
            int endColor2 = getColor(ResourceTable.Color_holo_purple);
            int endColor3 = getColor(ResourceTable.Color_holo_green_dark);
            int endColor4 = getColor(ResourceTable.Color_holo_red_dark);
            int endColor5 = getColor(ResourceTable.Color_holo_orange_dark);

            List<GradientColor> gradientColors = new ArrayList<>();
            gradientColors.add(new GradientColor(startColor1, endColor1));
            gradientColors.add(new GradientColor(startColor2, endColor2));
            gradientColors.add(new GradientColor(startColor3, endColor3));
            gradientColors.add(new GradientColor(startColor4, endColor4));
            gradientColors.add(new GradientColor(startColor5, endColor5));

            set1.setGradientColors(gradientColors);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
//            data.setValueTypeface(tfLight);
            data.setBarWidth(0.9f);

            chart.setData(data);
        }
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "BarChartActivity");
    }

    private final RectFloat onValueSelectedRectF = new RectFloat();

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;

        RectFloat bounds = onValueSelectedRectF;
        chart.getBarBounds((BarEntry) e, bounds);
        MPPointF position = chart.getPosition(e, YAxis.AxisDependency.LEFT);


        HiLog.info(lable, "bounds", bounds.toString());
        HiLog.info(lable, "position", position.toString());

        HiLog.info(lable, "x-index",
                "low: " + chart.getLowestVisibleX() + ", high: "
                        + chart.getHighestVisibleX());

        MPPointF.recycleInstance(position);
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
        tvX.setText(String.valueOf(seekBarX.getProgress()));
        tvY.setText(String.valueOf(seekBarY.getProgress()));

        setData(seekBarX.getProgress(), seekBarY.getProgress());
        chart.invalidate();
    }

    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }
}
