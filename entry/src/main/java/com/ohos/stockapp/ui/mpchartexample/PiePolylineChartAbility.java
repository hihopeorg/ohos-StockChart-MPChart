package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.net.Uri;

import java.util.ArrayList;

public class PiePolylineChartAbility extends DemoBase implements Slider.ValueChangedListener, OnChartValueSelectedListener {
    private PieChart chart;
    private Slider seekBarX, seekBarY;
    private Text tvX, tvY;
    private HiLogLabel lable=new HiLogLabel(HiLog.INFO,0x53673254, Chart.LOG_TAG);

    //    private Typeface tf;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_pie_polyline_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("PiePolylineChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("pie"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);
                                break;
                            case "actionToggleValues":
                                for (IDataSet<?> set : chart.getData().getDataSets())
                                    set.setDrawValues(!set.isDrawValuesEnabled());

                                chart.invalidate();
                                break;
                            case "actionToggleIcons": {
                                for (IDataSet<?> set : chart.getData().getDataSets())
                                    set.setDrawIcons(!set.isDrawIconsEnabled());

                                chart.invalidate();
                                break;
                            }
                            case "actionToggleHole": {
                                if (chart.isDrawHoleEnabled())
                                    chart.setDrawHoleEnabled(false);
                                else
                                    chart.setDrawHoleEnabled(true);
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleMinAngles": {
                                if (chart.getMinAngleForSlices() == 0f)
                                    chart.setMinAngleForSlices(36f);
                                else
                                    chart.setMinAngleForSlices(0f);
                                chart.notifyDataSetChanged();
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleCurvedSlices": {
                                boolean toSet = !chart.isDrawRoundedSlicesEnabled() || !chart.isDrawHoleEnabled();
                                chart.setDrawRoundedSlices(toSet);
                                if (toSet && !chart.isDrawHoleEnabled()) {
                                    chart.setDrawHoleEnabled(true);
                                }
                                if (toSet && chart.isDrawSlicesUnderHoleEnabled()) {
                                    chart.setDrawSlicesUnderHole(false);
                                }
                                chart.invalidate();
                                break;
                            }
                            case "actionDrawCenter": {
                                if (chart.isDrawCenterTextEnabled())
                                    chart.setDrawCenterText(false);
                                else
                                    chart.setDrawCenterText(true);
                                chart.invalidate();
                                break;
                            }
                            case "actionToggleXValues": {

                                chart.setDrawEntryLabels(!chart.isDrawEntryLabelsEnabled());
                                chart.invalidate();
                                break;
                            }
                            case "actionTogglePercent":
                                chart.setUsePercentValues(!chart.isUsePercentValuesEnabled());
                                chart.invalidate();
                                break;
                            case "animateX": {
                                chart.animateX(1400);
                                break;
                            }
                            case "animateY": {
                                chart.animateY(1400);
                                break;
                            }
                            case "animateXY": {
                                chart.animateXY(1400, 1400);
                                break;
                            }
                            case "actionToggleSpin": {
                                chart.spin(1000, chart.getRotationAngle(), chart.getRotationAngle() + 360, Easing.EaseInOutCubic);
                                break;
                            }
                            case "actionSave": {
                                saveToGallery();
                                break;
                            }
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        tvX = (Text) findComponentById(ResourceTable.Id_tvXMax);
        tvY = (Text) findComponentById(ResourceTable.Id_tvYMax);

        seekBarX = (Slider) findComponentById(ResourceTable.Id_seekBar1);
        seekBarY = (Slider) findComponentById(ResourceTable.Id_seekBar2);

        seekBarX.setValueChangedListener(this);
        seekBarY.setValueChangedListener(this);

        chart = (PieChart) findComponentById(ResourceTable.Id_chart1);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setExtraOffsets(5, 10, 5, 5);

        chart.setDragDecelerationFrictionCoef(0.95f);

//        tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

//        chart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        chart.setCenterText(generateCenterSpannableText());

        chart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);

        chart.setDrawHoleEnabled(true);
        chart.setHoleColor(Color.WHITE.getValue());

        chart.setTransparentCircleColor(Color.WHITE.getValue());
        chart.setTransparentCircleAlpha(110);

        chart.setHoleRadius(58f);
        chart.setTransparentCircleRadius(61f);

        chart.setDrawCenterText(true);

        chart.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart.setRotationEnabled(true);
        chart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        chart.setOnChartValueSelectedListener(this);

        seekBarX.setProgressValue(4);
        seekBarY.setProgressValue(100);

        chart.animateY(2000, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setEnabled(false);

    }

    private void setData(int count, float range) {

        ArrayList<PieEntry> entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count; i++) {
            entries.add(new PieEntry((float) (Math.random() * range) + range / 5, parties[i % parties.length]));
        }

        PieDataSet dataSet = new PieDataSet(entries, "Election Results");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);


        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);
        //dataSet.setUsingSliceColorAsValueLineColor(true);

        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK.getValue());
//        data.setValueTypeface(tf);
        chart.setData(data);

        // undo all highlights
        chart.highlightValues(null);

        chart.invalidate();
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(chart, "PiePolylineChartActivity");
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
        HiLog.info(lable, "VAL SELECTED",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }

    @Override
    public void onNothingSelected() {
        HiLog.info(lable,"PieChart", "nothing selected");
    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean b) {
        tvX.setText(String.valueOf(seekBarX.getProgress()));
        tvY.setText(String.valueOf(seekBarY.getProgress()));

        setData(seekBarX.getProgress(), seekBarY.getProgress());
    }
    private String generateCenterSpannableText() {

        StringBuffer s = new StringBuffer("MPohosChart\ndeveloped by Philipp Jahoda");
        return s.toString();
    }
    @Override
    public void onTouchStart(Slider slider) {

    }

    @Override
    public void onTouchEnd(Slider slider) {

    }
}
