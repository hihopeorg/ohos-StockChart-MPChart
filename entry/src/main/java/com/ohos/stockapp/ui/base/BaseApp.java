package com.ohos.stockapp.ui.base;

import ohos.aafwk.ability.AbilityPackage;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

public class BaseApp extends AbilityPackage {
    public static BaseApp mContext ; //获取到主线程的上下文
    public static EventHandler mAppHandler ; //获取到主线程的handler
//    public static Loop mAppLooper = null; //获取到主线程的looper
    public static Thread mMainThead ; //获取到主线程
    public static int mMainTheadId; //获取到主线程的id

    public static BaseApp getmContext() {
        return mContext;
    }

    public static void setmContext(BaseApp mContext) {
        BaseApp.mContext = mContext;
    }

    public static EventHandler getmAppHandler() {
        return mAppHandler;
    }

    public static void setmAppHandler(EventHandler mAppHandler) {
        BaseApp.mAppHandler = mAppHandler;
    }

    public static Thread getmMainThead() {
        return mMainThead;
    }

    public static void setmMainThead(Thread mMainThead) {
        BaseApp.mMainThead = mMainThead;
    }

    public static int getmMainTheadId() {
        return mMainTheadId;
    }

    public static void setmMainTheadId(int mMainTheadId) {
        BaseApp.mMainTheadId = mMainTheadId;
    }

    @Override
    public void onInitialize() {
        super.onInitialize();
//        mContext = this;
//        mAppHandler = new EventHandler(EventRunner.getMainEventRunner());
////        mAppLooper = getMainLooper();
//        mMainThead = Thread.currentThread();
//        mMainTheadId = myTid();//主線程id
    }
    public static BaseApp getApp() {
        return mContext;
    }

    public static EventHandler getAppHandler() {
        return mAppHandler;
    }

//    public static Looper getAppLooper() {
//        return mAppLooper;
//    }

    public static Thread getMainThread() {
        return mMainThead;
    }

    public static int getMainThreadId() {
        return mMainTheadId;
    }
}
