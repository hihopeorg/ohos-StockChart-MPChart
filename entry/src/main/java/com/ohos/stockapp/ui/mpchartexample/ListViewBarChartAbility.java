package com.ohos.stockapp.ui.mpchartexample;

import com.ohos.stockapp.ResourceTable;
import com.ohos.stockapp.ui.mpchartexample.notimportant.DemoBase;
import com.ohos.stockapp.ui.market.dialog.MyDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class ListViewBarChartAbility extends DemoBase {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.setUIContent(ResourceTable.Layout_ability_list_view_bar_chart);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setText("ListViewBarChartActivity");
        findComponentById(ResourceTable.Id_img).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                new MyDialog(getContext(), getList("only_github"), new MyDialog.MyDialogListener() {
                    @Override
                    public void DialogItemListener(String id, IDialog dialog) {
                        switch (id) {
                            case "viewGithub":
                                StringBuffer stringBuffer = new StringBuffer("and");
                                Intent intent = new Intent();
                                intent.setAction(stringBuffer.append("roid.intent.action.VIEW").toString());
                                Uri uri = Uri.parse(app_github_url);
                                intent.setUri(uri);
                                startAbility(intent);
                                break;
                        }
                        dialog.destroy();
                    }
                }).show();
            }
        });

    }

    @Override
    protected void onActive() {
        super.onActive();
        ListContainer lv = (ListContainer) findComponentById(ResourceTable.Id_listview1);

        ArrayList<BarData> list = new ArrayList<>();

        // 20 items
        for (int i = 0; i < 20; i++) {
            list.add(generateData(i + 1));
        }

        ChartDataAdapter cda = new ChartDataAdapter(getApplicationContext(), list);
        lv.setItemProvider(cda);
    }

    @Override
    protected void saveToGallery() {

    }

    private class ChartDataAdapter extends BaseItemProvider {

        List<BarData> list = new ArrayList<>();

        public ChartDataAdapter(Context context, List<BarData> objects) {
            list.clear();
            list.addAll(objects);
        }


        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            BarData data = (BarData) getItem(i);
            Component component1 = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_list_item_barchart, null, false);
            if (data != null) {
//                data.setValueTypeface(tfLight);
                data.setValueTextColor(Color.BLACK.getValue());
            }
            BarChart chart = (BarChart) component1.findComponentById(ResourceTable.Id_chart);
            chart.getDescription().setEnabled(false);
            chart.setDrawGridBackground(false);

            XAxis xAxis = chart.getXAxis();
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//            xAxis.setTypeface(tfLight);
            xAxis.setDrawGridLines(false);

            YAxis leftAxis = chart.getAxisLeft();
//            leftAxis.setTypeface(tfLight);
            leftAxis.setLabelCount(5, false);
            leftAxis.setSpaceTop(15f);

            YAxis rightAxis = chart.getAxisRight();
//            rightAxis.setTypeface(tfLight);
            rightAxis.setLabelCount(5, false);
            rightAxis.setSpaceTop(15f);

            // set data
            chart.setData(data);
            chart.setFitBars(true);

            // do not forget to refresh the chart
//            holder.chart.invalidate();
            chart.animateY(700);
            return component1;
        }
    }

    /**
     * generates a random ChartData object with just one DataSet
     *
     * @return Bar data
     */
    private BarData generateData(int cnt) {

        ArrayList<BarEntry> entries = new ArrayList<>();

        for (int i = 0; i < 12; i++) {
            entries.add(new BarEntry(i, (float) (Math.random() * 70) + 30));
        }

        BarDataSet d = new BarDataSet(entries, "New DataSet " + cnt);
        d.setColors(ColorTemplate.VORDIPLOM_COLORS);
        d.setBarShadowColor(Color.rgb(203, 203, 203));

        ArrayList<IBarDataSet> sets = new ArrayList<>();
        sets.add(d);

        BarData cd = new BarData(sets);
        cd.setBarWidth(0.9f);
        return cd;
    }
}
