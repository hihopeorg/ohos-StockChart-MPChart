package com.github.mikephil.charting.stockChart.charts;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import ohos.agp.components.Component;
import ohos.agp.components.DragEvent;
import ohos.agp.utils.Matrix;
import ohos.multimodalinput.event.MmiPoint;
 
public class CoupleChartGestureListener implements OnChartGestureListener {
    public static final int MSCALE_X = 0;   //!< use with getValues/setValues
    public static final int MSKEW_X  = 1;   //!< use with getValues/setValues
    public static final int MTRANS_X = 2;   //!< use with getValues/setValues
    public static final int MSKEW_Y  = 3;   //!< use with getValues/setValues
    public static final int MSCALE_Y = 4;   //!< use with getValues/setValues
    public static final int MTRANS_Y = 5;   //!< use with getValues/setValues
    public static final int MPERSP_0 = 6;   //!< use with getValues/setValues
    public static final int MPERSP_1 = 7;   //!< use with getValues/setValues
    public static final int MPERSP_2 = 8;   //!< use with getValues/setValues
    private static final String TAG = CoupleChartGestureListener.class.getSimpleName();
    private Chart srcChart;
    private Chart[] dstCharts;
    private CoupleClick coupleClick;

    public void setCoupleClick(CoupleClick coupleClick) {
        this.coupleClick = coupleClick;
    }

    public interface CoupleClick {
        void singleClickListener();
    }

    public CoupleChartGestureListener(Chart srcChart, Chart[] dstCharts) {
        this.srcChart = srcChart;
        this.dstCharts = dstCharts;
    }

    @Override
    public void onChartGestureStart(MmiPoint me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        syncCharts();
    }

    @Override
    public void onChartGestureEnd(MmiPoint me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        if (dstCharts == null) {
            return;
        }
        syncCharts();
        if (lastPerformedGesture != ChartTouchListener.ChartGesture.SINGLE_TAP) {
            srcChart.highlightValue(null,true);
//            for (Chart dstChart : dstCharts) {
//                if (dstChart != null) {
//                    dstChart.highlightValues(null);
//                }
//            }
        }
    }

    @Override
    public void onChartLongPressed(MmiPoint me) {
        syncCharts();
    }

    @Override
    public void onChartDoubleTapped(MmiPoint me) {
        syncCharts();
    }

    @Override
    public void onChartSingleTapped(MmiPoint me) {
        if (coupleClick != null) {
            coupleClick.singleClickListener();
        }
        syncCharts();
    }

    @Override
    public void onChartFling(MmiPoint me1, MmiPoint me2, float velocityX, float velocityY) {
        syncCharts();
    }

    @Override
    public void onChartScale(MmiPoint me, float scaleX, float scaleY) {
        syncCharts();
    }

    @Override
    public void onChartTranslate(DragEvent me, float dX, float dY) {

    }


    public void syncCharts() {
        if (dstCharts == null) {
            return;
        }
        Matrix srcMatrix;
        float[] srcVals = new float[9];
        Matrix dstMatrix;
        float[] dstVals = new float[9];
        // get src chart translation matrix:
        srcMatrix = srcChart.getViewPortHandler().getMatrixTouch();
        srcMatrix.getElements(srcVals);

        // apply X axis scaling and position to dst charts:
        for (Chart dstChart : dstCharts) {
            if (dstChart.getVisibility() == Component.VISIBLE) {
                dstMatrix = dstChart.getViewPortHandler().getMatrixTouch();
                dstMatrix.getElements(dstVals);

                dstVals[MSCALE_X] = srcVals[MSCALE_X];
                dstVals[MSKEW_X] = srcVals[MSKEW_X];
                dstVals[MTRANS_X] = srcVals[MTRANS_X];
                dstVals[MSKEW_Y] = srcVals[MSKEW_Y];
                dstVals[MSCALE_Y] = srcVals[MSCALE_Y];
                dstVals[MTRANS_Y] = srcVals[MTRANS_Y];
                dstVals[MPERSP_0] = srcVals[MPERSP_0];
                dstVals[MPERSP_1] = srcVals[MPERSP_1];
                dstVals[MPERSP_2] = srcVals[MPERSP_2];

                dstMatrix.setElements(dstVals);
                dstChart.getViewPortHandler().refresh(dstMatrix, dstChart, true);
            }
        }
    }

}
