package com.github.mikephil.charting.stockChart.charts;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.stockChart.dataManage.TimeDataManage;
import com.github.mikephil.charting.stockChart.markerView.LeftMarkerView;
import com.github.mikephil.charting.stockChart.markerView.TimeRightMarkerView;
import com.github.mikephil.charting.stockChart.renderer.TimeLineChartRenderer;
import com.github.mikephil.charting.stockChart.renderer.TimeXAxisRenderer;
import ohos.agp.components.AttrSet;
import ohos.agp.render.Canvas;
import ohos.app.Context;


public class TimeLineChart extends LineChart {
    private LeftMarkerView myMarkerViewLeft;
    private TimeRightMarkerView myMarkerViewRight;
    private TimeDataManage kTimeData;
    private VolSelected volSelected;

    public void setVolSelected(VolSelected volSelected) {
        this.volSelected = volSelected;
    }

    public interface VolSelected {
        void onVolSelected(int value);

        void onValuesSelected(double price, double upDown, int vol, double avg);
    }

    public TimeLineChart(Context context) {
        super(context);
    }

    public TimeLineChart(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public TimeLineChart(Context context, AttrSet attrs, String defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void initRenderer() {
        mRenderer = new TimeLineChartRenderer(this, mAnimator, mViewPortHandler);
    }

    @Override
    protected void initXAxisRenderer() {
        mXAxisRenderer = new TimeXAxisRenderer(mViewPortHandler, (TimeXAxis) mXAxis, mLeftAxisTransformer, this);
    }

    @Override
    public void initXAxis() {
        mXAxis = new TimeXAxis();
    }

    /*返回转型后的左右轴*/
    public void setMarker(LeftMarkerView markerLeft, TimeRightMarkerView markerRight, TimeDataManage kTimeData) {
        this.myMarkerViewLeft = markerLeft;
        this.myMarkerViewRight = markerRight;
        this.kTimeData = kTimeData;
    }

    @Override
    protected void drawMarkers(Canvas canvas) {
        // if there is no marker view or drawing marker is disabled
        if (!isDrawMarkersEnabled() || !valuesToHighlight()) {
            return;
        }

        for (int i = 0; i < mIndicesToHighlight.length; i++) {

            Highlight highlight = mIndicesToHighlight[i];

            IDataSet set = mData.getDataSetByIndex(highlight.getDataSetIndex());

            Entry e = mData.getEntryForHighlight(mIndicesToHighlight[i]);
            int entryIndex = set.getEntryIndex(e);

            // make sure entry not null
            if (e == null || entryIndex > set.getEntryCount() * mAnimator.getPhaseX()) {
                continue;
            }

            float[] pos = getMarkerPosition(highlight);

            // check bounds
            if (!mViewPortHandler.isInBounds(pos[0], pos[1])) {
                continue;
            }

            float yValForXIndex1 = (float) kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getNowPrice();
            float yValForXIndex2 = (float) kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getPer();

            if (volSelected != null) {
                volSelected.onVolSelected(kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getVolume());
                volSelected.onValuesSelected(kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getNowPrice(),
                        kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getPer(),
                        kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getVolume(),
                        kTimeData.getDatas().get((int) mIndicesToHighlight[i].getX()).getAveragePrice());
            }

            myMarkerViewLeft.setData(yValForXIndex1);
            myMarkerViewRight.setData(yValForXIndex2);

            myMarkerViewLeft.refreshContent(e, mIndicesToHighlight[i]);
            myMarkerViewRight.refreshContent(e, mIndicesToHighlight[i]);
            /*修复bug*/
            // invalidate();
            /*重新计算大小*/
            myMarkerViewLeft.estimateSize(EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT),EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT));
            myMarkerViewLeft.arrange(0, 0, myMarkerViewLeft.getEstimatedWidth(), myMarkerViewLeft.getEstimatedHeight());
            myMarkerViewRight.estimateSize(EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT),EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT));
            myMarkerViewRight.arrange(0, 0, myMarkerViewRight.getEstimatedWidth(), myMarkerViewRight.getEstimatedHeight());

            if (getAxisLeft().getLabelPosition() == YAxis.YAxisLabelPosition.OUTSIDE_CHART) {
                myMarkerViewLeft.draw(canvas, mViewPortHandler.contentLeft() - myMarkerViewLeft.getWidth() / 2, pos[1] + myMarkerViewLeft.getHeight() / 2);
            } else {
                myMarkerViewLeft.draw(canvas, mViewPortHandler.contentLeft() + myMarkerViewLeft.getWidth() / 2, pos[1] + myMarkerViewLeft.getHeight() / 2);
            }
            if (getAxisRight().getLabelPosition() == YAxis.YAxisLabelPosition.OUTSIDE_CHART) {
                myMarkerViewRight.draw(canvas, mViewPortHandler.contentRight() + myMarkerViewRight.getWidth() / 2, pos[1] + myMarkerViewRight.getHeight() / 2);//- myMarkerViewRight.getWidth()
            } else {
                myMarkerViewRight.draw(canvas, mViewPortHandler.contentRight() - myMarkerViewRight.getWidth() / 2, pos[1] + myMarkerViewRight.getHeight() / 2);//- myMarkerViewRight.getWidth()
            }
        }
    }
}
