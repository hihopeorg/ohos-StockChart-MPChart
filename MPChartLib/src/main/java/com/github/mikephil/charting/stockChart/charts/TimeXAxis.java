package com.github.mikephil.charting.stockChart.charts;


import com.github.mikephil.charting.components.XAxis;
import ohos.utils.PlainArray;

/**
 * Created by loro on 2017/2/8.
 */
public class TimeXAxis extends XAxis {
    private PlainArray<String> labels;

    public PlainArray<String> getXLabels() {
        return labels;
    }

    public void setXLabels(PlainArray<String> labels) {
        this.labels = labels;
    }
}
