
package com.github.mikephil.charting.animation;


import com.github.mikephil.charting.animation.Easing.EasingFunction;
import com.github.mikephil.charting.charts.Chart;
import ohos.agp.animation.Animator;

import ohos.agp.animation.AnimatorValue;


import java.text.DecimalFormat;

/**
 * Object responsible for all animations in the Chart. Animations require API level 11.
 *
 * @author Philipp Jahoda
 * @author Mick Ashton
 */
public class ChartAnimator implements Animator.StateChangedListener, AnimatorValue.ValueUpdateListener {

    private Chart mComponent;
    private DecimalFormat df;
    /**
     * object that is updated upon animation update
     */
    private boolean isPhaseX, isPhaseY = false;
    /**
     * The phase of drawn values on the y-axis. 0 - 1
     */
    @SuppressWarnings("WeakerAccess")
    protected float mPhaseY = 1f;

    /**
     * The phase of drawn values on the x-axis. 0 - 1
     */
    @SuppressWarnings("WeakerAccess")
    protected float mPhaseX = 1f;

    public ChartAnimator() {
    }


    public ChartAnimator(Chart component) {
        mComponent = component;
        df = new DecimalFormat("0.00");

    }


    private AnimatorValue xAnimator(int duration, EasingFunction easing) {

        AnimatorValue animatorX = new AnimatorValue();
        animatorX.setStateChangedListener(this);
        isPhaseX = true;
        animatorX.setValueUpdateListener(this);
//     TODO   .ofFloat(this, "phaseX", 0f, 1f);
        animatorX.setCurve(easing);
        animatorX.setDuration(duration);

        return animatorX;
    }


    private AnimatorValue yAnimator(int duration, EasingFunction easing) {

        AnimatorValue animatorY = new AnimatorValue();
        animatorY.setStateChangedListener(this);
        isPhaseY = true;
        animatorY.setValueUpdateListener(this);
//      TODO    .ofFloat(this, "phaseY", 0f, 1f);
        animatorY.setCurve(easing);
        animatorY.setDuration(duration);

        return animatorY;
    }

    /**
     * Animates values along the X axis, in a linear fashion.
     *
     * @param durationMillis animation duration
     */
    public void animateX(int durationMillis) {
        animateX(durationMillis, Easing.Linear);
    }

    /**
     * Animates values along the X axis.
     *
     * @param durationMillis animation duration
     * @param easing         EasingFunction
     */
    public void animateX(int durationMillis, EasingFunction easing) {
        AnimatorValue animatorX = xAnimator(durationMillis, easing);

        animatorX.start();

    }

    /**
     * Animates values along both the X and Y axes, in a linear fashion.
     *
     * @param durationMillisX animation duration along the X axis
     * @param durationMillisY animation duration along the Y axis
     */
    public void animateXY(int durationMillisX, int durationMillisY) {
        animateXY(durationMillisX, durationMillisY, Easing.Linear, Easing.Linear);
    }

    /**
     * Animates values along both the X and Y axes.
     *
     * @param durationMillisX animation duration along the X axis
     * @param durationMillisY animation duration along the Y axis
     * @param easing          EasingFunction for both axes
     */
    public void animateXY(int durationMillisX, int durationMillisY, EasingFunction easing) {
        AnimatorValue xAnimator = xAnimator(durationMillisX, easing);
        AnimatorValue yAnimator = yAnimator(durationMillisY, easing);

        xAnimator.start();
        yAnimator.start();


    }

    /**
     * Animates values along both the X and Y axes.
     *
     * @param durationMillisX animation duration along the X axis
     * @param durationMillisY animation duration along the Y axis
     * @param easingX         EasingFunction for the X axis
     * @param easingY         EasingFunction for the Y axis
     */
    public void animateXY(int durationMillisX, int durationMillisY, EasingFunction easingX,
                          EasingFunction easingY) {

        AnimatorValue xAnimator = xAnimator(durationMillisX, easingX);
        AnimatorValue yAnimator = yAnimator(durationMillisY, easingY);
        xAnimator.start();
        yAnimator.start();

    }

    /**
     * Animates values along the Y axis, in a linear fashion.
     *
     * @param durationMillis animation duration
     */
    public void animateY(int durationMillis) {
        animateY(durationMillis, Easing.Linear);
    }

    /**
     * Animates values along the Y axis.
     *
     * @param durationMillis animation duration
     * @param easing         EasingFunction
     */
    public void animateY(int durationMillis, EasingFunction easing) {
        AnimatorValue animatorY = yAnimator(durationMillis, easing);

        animatorY.start();

    }

    /**
     * Gets the Y axis phase of the animation.
     *
     * @return float value of {@link #mPhaseY}
     */
    public float getPhaseY() {
        return mPhaseY;
    }

    /**
     * Sets the Y axis phase of the animation.
     *
     * @param phase float value between 0 - 1
     */
    public void setPhaseY(float phase) {
        if (phase > 1f) {
            phase = 1f;
        } else if (phase < 0f) {
            phase = 0f;
        }
        mPhaseY = phase;
    }

    /**
     * Gets the X axis phase of the animation.
     *
     * @return float value of {@link #mPhaseX}
     */
    public float getPhaseX() {
        return mPhaseX;
    }

    /**
     * Sets the X axis phase of the animation.
     *
     * @param phase float value between 0 - 1
     */
    public void setPhaseX(float phase) {
        if (phase > 1f) {
            phase = 1f;
        } else if (phase < 0f) {
            phase = 0f;
        }
        mPhaseX = phase;
    }

    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {

    }

    @Override
    public void onEnd(Animator animator) {
        isPhaseX = false;
        isPhaseY = false;
        animator.cancel();
    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        if (isPhaseX) setPhaseX(v);
        if (isPhaseY) setPhaseY(v);
        mComponent.invalidate();
    }
}
