package com.github.mikephil.charting.jobs;

import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

/**
 * Created by Philipp Jahoda on 19/02/16.
 */
public abstract class AnimatedViewPortJob extends ViewPortJob implements AnimatorValue.ValueUpdateListener, Animator.StateChangedListener {

    protected AnimatorValue animator;

    protected float phase;

    protected float xOrigin;
    protected float yOrigin;

    public AnimatedViewPortJob(ViewPortHandler viewPortHandler, float xValue, float yValue, Transformer trans, Component v, float xOrigin, float yOrigin, long duration) {
        super(viewPortHandler, xValue, yValue, trans, v);
        this.xOrigin = xOrigin;
        this.yOrigin = yOrigin;
        animator =new AnimatorValue();
        animator.setDuration(duration);
        animator.setValueUpdateListener(this);
        animator.setStateChangedListener(this);
    }

    @Override
    public void run() {
        animator.start();
    }

    public float getPhase() {
        return phase;
    }

    public void setPhase(float phase) {
        this.phase = phase;
    }

    public float getXOrigin() {
        return xOrigin;
    }

    public float getYOrigin() {
        return yOrigin;
    }

    public abstract void recycleSelf();

    protected void resetAnimator() {
        animator.stop();
        animator.release();
        animator.setValueUpdateListener(this);
        animator.setStateChangedListener(this);
    }

    @Override
    public void onStart(Animator animator) {

    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {
        try {
            recycleSelf();
        } catch (IllegalArgumentException e) {
            // don't worry about it.
        }
    }

    @Override
    public void onEnd(Animator animator) {
        try {
            recycleSelf();
        } catch (IllegalArgumentException e) {
            // don't worry about it.
        }
    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        setPhase(v);
    }
}
