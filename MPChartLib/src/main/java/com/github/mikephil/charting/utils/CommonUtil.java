package com.github.mikephil.charting.utils;


import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtil {

	public static int getWindowWidth(Context context){
		DisplayAttributes displayAttributes=new DisplayAttributes();

		return displayAttributes.width;
	}

	public static int getWindowHeight(Context context){
		DisplayAttributes displayAttributes=new DisplayAttributes();
		return displayAttributes.width;
	}

    /**
     * 功能：判断一个字符串是否包含特殊字符
     * @param string 要判断的字符串
     * @return true 提供的参数string不包含特殊字符
     * @return false 提供的参数string包含特殊字符
     */
    public static boolean isConSpeCharacters(String string) {
        if(string.replaceAll("[\u4e00-\u9fa5]*[a-z]*[A-Z]*\\d*-*_*\\s*", "").length()==0){
            //如果不包含特殊字符
            return true;
        }
        return false;
    }

    public static boolean isConCharacters(String string){
        if(string.replaceAll("[a-z]*[A-Z]*\\d*-*_*\\s*", "").length()==0){
            return true;
        }else{
            return false;
        }
    }

    //浮点型判断
	public static boolean isDecimal(String str) {
		if(str==null || "".equals(str))
			return false;
		Pattern pattern = Pattern.compile("[0-9]*(\\.?)[0-9]*");
		return pattern.matcher(str).matches();
	}

    //整形判断
	public static boolean isNumeric(String str){
		for (int i = str.length();--i>=0;){
			if (!Character.isDigit(str.charAt(i))){
				return false;
			}
		}
		return true;
	}

	public static int getStatusBarHeight(Context context) {
		int result = 0;
		Point point =new Point();
		Point point2 =new Point();
		DisplayManager.getInstance().getDefaultDisplay(context).get().getRealSize(point);
		DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point2);
		result= (int) (point.getPointY()-point2.getPointY());


		return result;
	}
	public static int sp2px(Context context, float spValue) {

		return AttrHelper.fp2px(spValue,context);
	}

	public static int dip2px(Context context, float dipValue) {
		return AttrHelper.vp2px(dipValue,context);
	}

    public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern.compile("[1][34578]\\d{9}");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static boolean isChar(String mobiles) {
		Pattern p = Pattern.compile("[a-zA-Z]");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static boolean isNUM(String mobiles) {
		Pattern p = Pattern.compile("[1-9]");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	public static boolean isZeroString(String str) {
		if (str == null || "".equals(str)) {
			return true;
		}
		Pattern p = Pattern.compile("[0]+");
		Matcher m = p.matcher(str);
		return m.matches();
	}

	/**
	 * 计算TextView 的宽度
	 * @param textView
	 * @param text
	 * @return
	 */
	/*public static float getTextViewLength(TextView textView,String text){
		TextPaint paint = textView.getPaint();
		// 得到使用该paint写上text的时候,像素为多少
		float textLength = paint.measureText(text);
		return textLength;  
	}*/

    /**根据最大长度获取文本需要设置的字体*/
   /* public static int getTextSize(TextView textView,String text,int allLength){
        int size = 13;
        TextPaint paint = textView.getPaint();
        while (paint.measureText(text) >= allLength){
            size -= 1;
            textView.setTextSize(size);
        }
        return size;
    }
*/

}
