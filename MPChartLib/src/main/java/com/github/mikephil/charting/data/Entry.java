
package com.github.mikephil.charting.data;
import com.github.mikephil.charting.utils.Utils;
import ohos.agp.components.element.Element;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * Class representing one entry in the chart. Might contain multiple values.
 * Might only contain a single value depending on the used constructor.
 *
 * @author Philipp Jahoda
 */
public class Entry extends BaseEntry implements Sequenceable {

    /**
     * the x value
     */
    private float x = 0f;

    public Entry() {

    }

    /**
     * A Entry represents one single entry in the chart.
     *
     * @param x the x value
     * @param y the y value (the actual value of the entry)
     */
    public Entry(float x, float y) {
        super(y);
        this.x = x;
    }

    /**
     * A Entry represents one single entry in the chart.
     *
     * @param x the x value
     * @param y the y value (the actual value of the entry)
     * @param data Spot for additional data this Entry represents.
     */
    public Entry(float x, float y, Object data) {
        super(y, data);
        this.x = x;
    }

    /**
     * A Entry represents one single entry in the chart.
     *
     * @param x    the x value
     * @param y    the y value (the actual value of the entry)
     * @param icon icon image
     */
    public Entry(float x, float y, Element icon) {
        super(y, icon);
        this.x = x;
    }

    /**
     * A Entry represents one single entry in the chart.
     *
     * @param x    the x value
     * @param y    the y value (the actual value of the entry)
     * @param icon icon image
     * @param data Spot for additional data this Entry represents.
     */
    public Entry(float x, float y, Element icon, Object data) {
        super(y, icon, data);
        this.x = x;
    }

    /**
     * Returns the x-value of this Entry object.
     *
     * @return
     */
    public float getX() {
        return x;
    }

    /**
     * Sets the x-value of this Entry object.
     *
     * @param x
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * returns an exact copy of the entry
     *
     * @return
     */
    public Entry copy() {
        Entry e = new Entry(x, getY(), getData());
        return e;
    }

    /**
     * Compares value, xIndex and data of the entries. Returns true if entries
     * are equal in those points, false if not. Does not check by hash-code like
     * it's done by the "equals" method.
     *
     * @param e
     * @return
     */
    public boolean equalTo(Entry e) {

        if (e == null) {
            return false;
        }

        if (e.getData() != this.getData()) {
            return false;
        }

        if (Math.abs(e.x - this.x) > Utils.FLOAT_EPSILON) {
            return false;
        }

        return !(Math.abs(e.getY() - this.getY()) > Utils.FLOAT_EPSILON);
    }

    /**
     * returns a string representation of the entry containing x-index and value
     */
    @Override
    public String toString() {
        return "Entry, x: " + x + " y: " + getY();
    }



    protected Entry(Parcel in) {
        this.x = in.readFloat();
        this.setY(in.readFloat());
        if (in.readInt() == 1) {
            this.setData(in.readSequenceable(this));
        }
    }

    public static final Sequenceable.Producer<Entry> CREATOR = new Sequenceable.Producer<Entry>() {
        @Override
        public Entry createFromParcel(Parcel source) {
            return new Entry(source);
        }
    };

    @Override
    public boolean marshalling(Parcel parcel) {
        parcel.writeFloat(x);
        parcel.writeFloat(getY());
        parcel.writeSequenceable(this);
        return true;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.x= parcel.readFloat();
        setY(parcel.readFloat());
        parcel.readSequenceable(this);
        return true;
    }
}
