package com.github.mikephil.charting.stockChart.markerView;



import com.github.mikephil.charting.ResourceTable;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Created by Administrator on 2016/9/12.
 */
public class BarBottomMarkerView extends MarkerView {

    private Text markerTv;
    private String date;

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     * @param layoutResource the layout resource to use for the MarkerView
     */
    public BarBottomMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        markerTv = (Text) findComponentById(ResourceTable.Id_marker_tv);
        markerTv.setTextSize(10);
    }

    public void setData(String date) {
        this.date = date;
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        markerTv.setText(date);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }

}
