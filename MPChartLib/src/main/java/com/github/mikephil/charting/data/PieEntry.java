package com.github.mikephil.charting.data;


import ohos.agp.components.element.Element;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * @author Philipp Jahoda
 */
public class PieEntry extends Entry {

    private String label;
    private HiLogLabel labelstr=new HiLogLabel(HiLog.INFO,0X56567432,"DEPRECATED");

    public PieEntry(float value) {
        super(0f, value);
    }

    public PieEntry(float value, Object data) {
        super(0f, value, data);
    }

    public PieEntry(float value, Element icon) {
        super(0f, value, icon);
    }

    public PieEntry(float value, Element icon, Object data) {
        super(0f, value, icon, data);
    }

    public PieEntry(float value, String label) {
        super(0f, value);
        this.label = label;
    }

    public PieEntry(float value, String label, Object data) {
        super(0f, value, data);
        this.label = label;
    }

    public PieEntry(float value, String label, Element icon) {
        super(0f, value, icon);
        this.label = label;
    }

    public PieEntry(float value, String label, Element icon, Object data) {
        super(0f, value, icon, data);
        this.label = label;
    }

    /**
     * This is the same as getY(). Returns the value of the PieEntry.
     *
     * @return
     */
    public float getValue() {
        return getY();
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Deprecated
    @Override
    public void setX(float x) {
        super.setX(x);
        HiLog.info(labelstr, "Pie entries do not have x values");
    }

    @Deprecated
    @Override
    public float getX() {
        HiLog.info(labelstr, "Pie entries do not have x values");
        return super.getX();
    }

    @Override
    public PieEntry copy() {
        PieEntry e = new PieEntry(getY(), label, getData());
        return e;
    }
}
