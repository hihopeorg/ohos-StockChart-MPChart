package com.github.mikephil.charting.animation;
import ohos.agp.animation.Animator;

/**
 * Interface for creating custom made easing functions. Uses the
 * TimeInterpolator interface provided by .
 */
public interface EasingFunction extends Animator.TimelineCurve {

    @Override
    float getCurvedTime(float v);
}
