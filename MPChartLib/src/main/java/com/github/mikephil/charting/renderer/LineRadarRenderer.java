package com.github.mikephil.charting.renderer;


import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import ohos.agp.components.element.Element;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.media.image.PixelMap;

/**
 * Created by Philipp Jahoda on 25/01/16.
 */
public abstract class LineRadarRenderer extends LineScatterCandleRadarRenderer {

    public LineRadarRenderer(ChartAnimator animator, ViewPortHandler viewPortHandler) {
        super(animator, viewPortHandler);
    }

    /**
     * Draws the provided path in filled mode with the provided drawable.
     *
     * @param c
     * @param filledPath
     * @param drawable
     */
    protected void drawFilledPath(Canvas c, Path filledPath, Element drawable) {

        if (clipPathSupported()) {

        int save = c.save();
        c.clipPath(filledPath, Canvas.ClipOp.INTERSECT);

       drawable.setBounds((int) mViewPortHandler.contentLeft(),
                (int) mViewPortHandler.contentTop(),
                (int) mViewPortHandler.contentRight(),
                (int) mViewPortHandler.contentBottom());


       drawable.drawToCanvas(c);
        c.restoreToCount(save);
        } else {
            throw new RuntimeException("Fill-drawables not (yet) supported below API level 18, " +
                    "this code was run on API level .");
        }
    }

    /**
     * Draws the provided path in filled mode with the provided color and alpha.
     * Special thanks to Angelo Suzuki (https://github.com/tinsukE) for this.
     *
     * @param c
     * @param filledPath
     * @param fillColor
     * @param fillAlpha
     */
    protected void drawFilledPath(Canvas c, Path filledPath, int fillColor, int fillAlpha) {

        int color = (fillAlpha << 24) | (fillColor & 0xffffff);

        if (clipPathSupported()) {

            int save = c.save();

            c.clipPath(filledPath, Canvas.ClipOp.INTERSECT);

            c.drawColor(color, BlendMode.COLOR);
            c.restoreToCount(save);
        } else {

            // save
            Paint.Style previous = mRenderPaint.getStyle();
            int previousColor = mRenderPaint.getColor().getValue();

            // set
            mRenderPaint.setStyle(Paint.Style.FILL_STYLE);
            mRenderPaint.setColor(new Color(color));

            c.drawPath(filledPath, mRenderPaint);

            // restore
            mRenderPaint.setColor(new Color(previousColor));
            mRenderPaint.setStyle(previous);
        }
    }

    /**
     * Clip path with hardware acceleration only working properly on API level 18 and above.
     *
     * @return
     */
    private boolean clipPathSupported() {
        return Utils.getSDKInt() >= 18;
    }
}
