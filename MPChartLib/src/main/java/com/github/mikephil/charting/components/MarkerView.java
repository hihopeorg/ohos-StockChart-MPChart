package com.github.mikephil.charting.components;

import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import java.lang.ref.WeakReference;

/**
 * View that can be displayed when selecting values in the chart. Extend this class to provide custom layouts for your
 * markers.
 *
 * @author Philipp Jahoda
 */
public class MarkerView extends DependentLayout implements ComponentContainer.ArrangeListener, IMarker, Component.DrawTask, Component.EstimateSizeListener {

    private final Context mContext;
    private MPPointF mOffset = new MPPointF();
    private MPPointF mOffset2 = new MPPointF();
    private WeakReference<Chart> mWeakChart;

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     * @param layoutResource the layout resource to use for the MarkerView
     */
    public MarkerView(Context context, int layoutResource) {
        super(context);
        mContext = context;
        setupLayoutResource(layoutResource);
        addDrawTask(this);
        setArrangeListener(this);
        setEstimateSizeListener(this);
    }

    /**
     * Sets the layout resource for a custom MarkerView.
     *
     * @param layoutResource
     */
    private void setupLayoutResource(int layoutResource) {

        Component inflated = LayoutScatter.getInstance(mContext).parse(layoutResource, this, true);

        inflated.setLayoutConfig(new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT));
        inflated.estimateSize(EstimateSpec.getSizeWithMode(0, EstimateSpec.NOT_EXCEED), EstimateSpec.getSizeWithMode(0, EstimateSpec.NOT_EXCEED));

//        estimateSize(getWidth(), getHeight());
        inflated.arrange(0, 0, inflated.getEstimatedWidth(), inflated.getEstimatedHeight());
    }

    public void setOffset(MPPointF offset) {
        mOffset = offset;

        if (mOffset == null) {
            mOffset = new MPPointF();
        }
    }

    public void setOffset(float offsetX, float offsetY) {
        mOffset.x = offsetX;
        mOffset.y = offsetY;
    }

    @Override
    public MPPointF getOffset() {
        return mOffset;
    }

    public void setChartView(Chart chart) {
        mWeakChart = new WeakReference<>(chart);
    }

    public Chart getChartView() {
        return mWeakChart == null ? null : mWeakChart.get();
    }

    @Override
    public MPPointF getOffsetForDrawingAtPoint(float posX, float posY) {

        MPPointF offset = getOffset();
        mOffset2.x = offset.x;
        mOffset2.y = offset.y;

        Chart chart = getChartView();

        float width = getWidth();
        float height = getHeight();

        if (posX + mOffset2.x < 0) {
            mOffset2.x = -posX;
        } else if (chart != null && posX + width + mOffset2.x > chart.getWidth()) {
            mOffset2.x = chart.getWidth() - posX - width;
        }

        if (posY + mOffset2.y < 0) {
            mOffset2.y = -posY;
        } else if (chart != null && posY + height + mOffset2.y > chart.getHeight()) {
            mOffset2.y = chart.getHeight() - posY - height;
        }

        return mOffset2;
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        estimateSize(EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT),
                EstimateSpec.getSizeWithMode(0, EstimateSpec.UNCONSTRAINT));
        arrange(0, 0, getEstimatedWidth(), getEstimatedHeight());
    }



    @Override
    public void draw(Canvas canvas, float posX, float posY) {

        MPPointF offset = getOffsetForDrawingAtPoint(posX, posY);

        int saveId = canvas.save();
        // translate to the correct position and draw
        canvas.translate(posX + offset.x, posY + offset.y);
        onDraw(this,canvas);
        canvas.restoreToCount(saveId);
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {

    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        return false;
    }

    @Override
    public boolean onArrange(int i, int i1, int i2, int i3) {
        return false;
    }
}
