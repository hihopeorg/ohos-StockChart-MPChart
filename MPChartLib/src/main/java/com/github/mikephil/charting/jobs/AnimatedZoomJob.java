package com.github.mikephil.charting.jobs;



import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.utils.ObjectPool;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.ViewPortHandler;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.utils.Matrix;

/**
 * Created by Philipp Jahoda on 19/02/16.
 */

public class AnimatedZoomJob extends AnimatedViewPortJob implements Animator.StateChangedListener {

    private static ObjectPool<AnimatedZoomJob> pool;

    static {
        pool = ObjectPool.create(8, new AnimatedZoomJob(null, null, null, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
    }

    public static AnimatedZoomJob getInstance(ViewPortHandler viewPortHandler, Component v, Transformer trans, YAxis axis, float xAxisRange, float scaleX, float scaleY, float xOrigin, float yOrigin, float zoomCenterX, float zoomCenterY, float zoomOriginX, float zoomOriginY, long duration) {
        AnimatedZoomJob result = pool.get();
        result.mViewPortHandler = viewPortHandler;
        result.xValue = scaleX;
        result.yValue = scaleY;
        result.mTrans = trans;
        result.view = v;
        result.xOrigin = xOrigin;
        result.yOrigin = yOrigin;
        result.yAxis = axis;
        result.xAxisRange = xAxisRange;
        result.resetAnimator();
        result.animator.setDuration(duration);
        return result;
    }

    protected float zoomOriginX;
    protected float zoomOriginY;

    protected float zoomCenterX;
    protected float zoomCenterY;

    protected YAxis yAxis;

    protected float xAxisRange;

    public AnimatedZoomJob(ViewPortHandler viewPortHandler, Component v, Transformer trans, YAxis axis, float xAxisRange, float scaleX, float scaleY, float xOrigin, float yOrigin, float zoomCenterX, float zoomCenterY, float zoomOriginX, float zoomOriginY, long duration) {
        super(viewPortHandler, scaleX, scaleY, trans, v, xOrigin, yOrigin, duration);

        this.zoomCenterX = zoomCenterX;
        this.zoomCenterY = zoomCenterY;
        this.zoomOriginX = zoomOriginX;
        this.zoomOriginY = zoomOriginY;
        this.animator.setStateChangedListener(this);
        this.yAxis = axis;
        this.xAxisRange = xAxisRange;
    }

    protected Matrix mOnAnimationUpdateMatrixBuffer = new Matrix();

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        super.onUpdate(animatorValue, v);

        float scaleX = xOrigin + (xValue - xOrigin) * phase;
        float scaleY = yOrigin + (yValue - yOrigin) * phase;

        Matrix save = mOnAnimationUpdateMatrixBuffer;
        mViewPortHandler.setZoom(scaleX, scaleY, save);
        mViewPortHandler.refresh(save, view, false);

        float valsInView = yAxis.mAxisRange / mViewPortHandler.getScaleY();
        float xsInView = xAxisRange / mViewPortHandler.getScaleX();

        pts[0] = zoomOriginX + ((zoomCenterX - xsInView / 2f) - zoomOriginX) * phase;
        pts[1] = zoomOriginY + ((zoomCenterY + valsInView / 2f) - zoomOriginY) * phase;

        mTrans.pointValuesToPixel(pts);

        mViewPortHandler.translate(pts, save);
        mViewPortHandler.refresh(save, view, true);
    }

    @Override
    public void onStart(Animator animator) {
        super.onStart(animator);
    }

    @Override
    public void onStop(Animator animator) {
        super.onStop(animator);
    }

    @Override
    public void onCancel(Animator animator) {
        super.onCancel(animator);
    }

    @Override
    public void onEnd(Animator animator) {
        super.onEnd(animator);
        ((BarLineChartBase) view).calculateOffsets();
        view.postLayout();
    }

    @Override
    public void onPause(Animator animator) {
        super.onPause(animator);
    }

    @Override
    public void onResume(Animator animator) {
        super.onResume(animator);
    }



    @Override
    public void recycleSelf() {

    }



    @Override
    protected ObjectPool.Poolable instantiate() {
        return new AnimatedZoomJob(null, null, null, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    }
}
